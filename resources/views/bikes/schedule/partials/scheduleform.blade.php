<section class="scheduleform-day">
	<section class="scheduleform-day-checkbox">
		{!! Form::label('monday', 'Monday', ['class' => '']) !!}
		{!! Form::checkbox('monday','true', $schedule->monday, ['class' => 'daycheckbox']) !!}
	</section>
</section>
<section class="scheduleform-day">
	<section class="scheduleform-day-checkbox">
		{!! Form::label('tuesday', 'Tuesday', ['class' => '']) !!}
		{!! Form::checkbox('tuesday','true', $schedule->tuesday, ['class' => 'daycheckbox']) !!}
	</section>
</section>
<section class="scheduleform-day">
	<section class="scheduleform-day-checkbox">
		{!! Form::label('wednesday', 'Wednesday', ['class' => '']) !!}
		{!! Form::checkbox('wednesday','true', $schedule->wednesday, ['class' => 'daycheckbox']) !!}
	</section>
</section>
<section class="scheduleform-day">
	<section class="scheduleform-day-checkbox">
		{!! Form::label('thursday', 'Thursday', ['class' => '']) !!}
		{!! Form::checkbox('thursday','true', $schedule->thursday, ['class' => 'daycheckbox']) !!}
	</section>
</section>
<section class="scheduleform-day">
	<section class="scheduleform-day-checkbox">
		{!! Form::label('friday', 'Friday', ['class' => '']) !!}
		{!! Form::checkbox('friday','true', $schedule->friday, ['class' => 'daycheckbox']) !!}
	</section>
</section>
<section class="scheduleform-day">
	<section class="scheduleform-day-checkbox">
		{!! Form::label('saturday', 'Saturday', ['class' => '']) !!}
		{!! Form::checkbox('saturday','true', $schedule->saturday, ['class' => 'daycheckbox']) !!}
	</section>
</section>
<section class="scheduleform-day">
	<section class="scheduleform-day-checkbox">
		{!! Form::label('sunday', 'Sunday', ['class' => '']) !!}
		{!! Form::checkbox('sunday','true', $schedule->sunday, ['class' => 'daycheckbox']) !!}
	</section>
</section>