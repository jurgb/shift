@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	<section class="container">
		<h1 class="formheader">Binnenkort kan je hier kiezen wanneer je jouw fiets wilt verhuren.</h1>
		@include('errors.formerrors')
		{!! Form::open(array('url' => '/bikes/' . $bike->id . '/schedule/create', 'method' => 'post')) !!}
			{{ csrf_field() }}
			<!--@include('bikes.schedule.partials.scheduleform', ['schedule' => $schedule])-->
			<section class="form-button-wrapper align-button-right">
				{!! Form::submit('Volgende', ['class' => 'btn btn-default'])!!}
			</section>
		{!! Form::close() !!}
	</section>
@stop

@section('scripts')
@stop