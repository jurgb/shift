@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	<section class="bike_photopreview_wrapper">
		<section class="container">
			<h1 class="formheader">Klik op een foto om deze als coverfoto in te stellen</h1>
			@foreach ($bike->photos as $photo)
				<a class="bike-show-image-gallery-link" href="{{url('/bikes/'. $bike->id . '/changecover/' . $photo->id)}}"><img src="/{{$photo->thumbnail_path}}" /></a>
			@endforeach
		</section>
	</section>
@stop

@section('scripts')
@stop