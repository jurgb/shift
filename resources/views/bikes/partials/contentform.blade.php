<section class="form-labeled-field-full">
	{!! Form::label('title', 'Geef een naam aan je fiets') !!}
	{!! Form::text('title', $bike->title, ['class' => 'input_bottom_border', 'placeholder' => 'vb. Blauwe shift fiets']) !!}
</section>
<section class="form-labeled-select-full">
	{!! Form::label('type', 'Type fiets:') !!}
	{!! Form::select('type',   ['' => 'Select type of bike','stadsfiets' => 'Stadsfiets', 'damesfiets' => 'Damesfiets','herenfiets' => 'Herenfiets', 'koersfiets' => 'Koersfiets', 'kinderfiets' => 'Kinderfiets', 'fixie' => 'Fixie','mountainbike' => 'Mountainbike'], $bike->type, ['class' => 'input_bottom_border']) !!}
</section>
<section class="form-labeled-textarea-full">
	{!! Form::label('description', 'Omschrijving') !!}
	{!! Form::textarea('description', $bike->description, ['class' => 'input_bottom_border', 'placeholder' => 'Vertel iets meer over je fiets en waarom je hem wil verhuren', 'rows' => '3']) !!}
</section>
<section class="form-labeled-field-full">
	{!! Form::label('street', 'Straat') !!}
	{!! Form::text('street', $bike->street, ['class' => 'input_bottom_border', 'placeholder' => 'vb. Lange Ridderstraat']) !!}
</section>
<section class="form-labeled-field-full">
	{!! Form::label('streetnumber', 'Huisnummer') !!}
	{!! Form::text('streetnumber', $bike->streetnumber, ['class' => 'input_bottom_border', 'placeholder' => 'vb. 44']) !!}
</section>
<section class="form-labeled-field-full">
	{!! Form::label('zipcode', 'Postcode') !!}
	{!! Form::text('zipcode', $bike->zipcode, ['class' => 'input_bottom_border', 'placeholder' => 'vb. 2800']) !!}
</section>
<section class="form-labeled-field-full">
	{!! Form::label('city', 'Gemeente/stad') !!}
	{!! Form::text('city', $bike->city, ['class' => 'input_bottom_border', 'placeholder' => 'vb. Mechelen']) !!}
</section>
<section class="form-labeled-field-full notvisible">
	{!! Form::label('price', 'Vraagprijs per uur') !!}
	{!! Form::text('price', str_replace(".",",",$bike->price), ['class' => 'input_bottom_border', 'value' => '10,00']) !!}
</section>