{!! Form::open(['url' => 'bikes', 'method' => 'get', 'class' => '']) !!}
	<section class="bike-filter">
		{!! Form::label('type', 'Type fiets:') !!}
		{!! Form::select('type',   ['' => 'Selecteer type fiets','stadsfiets' => 'Stadsfiets', 'damesfiets' => 'Damesfiets','herenfiets' => 'Herenfiets', 'koersfiets' => 'Koersfiets', 'kinderfiets' => 'Kinderfiets', 'fixie' => 'Fixie','mountainbike' => 'Mountainbike'], $filterType, ['class' => '']) !!}
	</section>
	<section class="bike-filter">
		{!! Form::label('city', 'Gemeente') !!}
		{!! Form::text('city', $filterCity, ['class' => 'input_bottom_border', 'placeholder' => 'vb. Mechelen']) !!}
	</section>
	<section class="bike-filter">
		{!! Form::label('maxprice', 'Maximum price') !!}
		{!! Form::number('maxprice', $filtermaxprice, ['class' => 'input_bottom_border', 'placeholder' => 'vb. 10', 'min' => "0"]) !!}
	</section>
  <div>
    {!! Form::submit('Zoek', ['class' => 'btn btn-default-inverse']) !!}
    <a href="{!! url('/bikes') !!}" class="sparkle">Empty filter</a>
  </div>

{!! Form::close() !!}


@section('filter.scripts')

@stop