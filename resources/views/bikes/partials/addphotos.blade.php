<section class="form-labeled-fileuploader-full">
	{!! Form::label('imgInp', "Kies foto's voor je fiets", ['class' => 'btn btn-small btn-secondary', 'style' => 'margin-top:25px;']) !!}
	<span class="guidelines">Ondersteunde foto formaten zijn: jpg,jpeg,png,bmp,gif</span>
	<span class="guidelines">Maximum bestandgrootte is 3MB</span>
	{!! Form::file('bikepictures[]',['multiple', 'class' =>'form-input-full', 'id' => 'imgInp', 'style' => 'display:none']) !!}
	<div id="image_previewer">
	</div>
</section>