@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	<section class="container">
		@if($bike->photos->count())
			<section class="bike_photopreview_wrapper">
				<h1 class="formheader">Deze foto's heb je reeds hebt toegevoegd</h1>
				<section class="container">
					@foreach ($bike->photos as $photo)
						<img src="/{{$photo->thumbnail_path}}" />
					@endforeach
				</section>
			</section>
			<h1 class="formheader">Voeg meer foto's van je fiets toe!</h1>
		@else
			<h1 class="formheader">Voeg foto's van je fiets toe!</h1>
		@endif
		<section class="container align-content-center">
			@include('errors.formerrors')
			{!! Form::open(array('url' => '/bikes/' . $bike->id . '/addphotos', 'method' => 'post','files' => true)) !!}
				{{ csrf_field() }}
				@include('bikes.partials.addphotos')
				<section class="form-button-wrapper align-button-right">
					{!! Form::submit("Voeg foto's toe", ['class' => 'btn btn-default'])!!}
				</section>
			{!! Form::close() !!}
		</section>
	</section>
@stop

@section('scripts')
@stop
	

	