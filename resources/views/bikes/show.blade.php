@extends('layouts.default')
@section('navigation')

@stop


@section('headerimage')
	<section class="headerimage" @if($bike->image) style="background-image:url(/{{$bike->image->path}})" @endif>
		<hgroup>
			<h2 class="bike-title">{{ $bike->title }}</h2>
			<h3 class="bike-city"><i class="fa fa-map-marker"></i>
				<span>{{ $bike->city }}</span> -
				<span>{{$bike->user->firstname}} {{$bike->user->lastname}}</span>
			</h3>
		</hgroup>
	</section>
@stop
@section('content')
	<section class="bike-show-header">
		<section class="bike-show-header-content">
			<section class="bike-show-header-owner-wrapper">
				@if($bike->user->image)
					<span class="bike-owner-picture" style="background-image:url(/{{$bike->user->image->thumbnail_path}});">
				@else
					<span class="bike-owner-picture" style="background-image:url({!! asset('/images/placeholders/user-default.png') !!});">
				@endif
				</span>
				<a class="user-name-profile btn btn-default-inverse" href="{{url('users/show/' . $bike->user->id)}}"><i class="fa fa-user"></i><span class="bike-owner-name">{{$bike->user->firstname}} {{$bike->user->lastname}}</span></a>
			</section>
			<section class="bike-show-header-actions">
				
				<!--@if(Auth::User())
					<a class="btn btn-social-action-small" href="#comment-section"> {{$bike->comments->count()}} <i class="fa fa-comment"></i></a>
					@if(Auth::User()->id === $bike->user->id)
						<a class="btn btn-social-action-small" href="#"> {{$bike->likes->count()}} <i class="fa fa-heart"></i></a>
						<a class="btn btn-small btn-default-inverse" href="{{url('/bikes/'. $bike->id . '/edit')}}">Bewerken</a>
						<a class="btn btn-small btn-default-inverse" href="{{url('/bikes/'. $bike->id . '/schedule/edit')}}">Schema Aanpassen</a>
						<a class="btn btn-small btn-default-inverse" href="{{url('/bikes/'. $bike->id . '/addphotos')}}">Foto's Toevoegen</a>
						@if($bike->photos()->count() > 1)
							<a class="btn btn-small btn-default-inverse" href="{{url('/bikes/'. $bike->id . '/changecover')}}">Coverfoto aanpassen</a>
						@endif
						{!!Form::open(['url' => '/bikes/'. $bike->id, 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) !!}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-small btn-default-inverse">Verwijderen</button>
						{!! Form::close() !!}
					@else
						<a class="btn btn-social-action-small btn_like_bike" href="{{url('/bikes/' . $bike->id . '/like')}}"> 
							{{$bike->likes->count()}} 
							@if($bike->likes()->where('user_id', Auth::user()->id)->first())	
								<i class="fa fa-heart"></i>
							@else
								<i class="fa fa-heart-o"></i>
							@endif
						</a>
						<a class="btn btn-small btn-default-inverse" href="{{url('/bikes/'. $bike->id . '/order')}}">Rent this bike</a>
						<a class="btn btn-small btn-default-inverse" href="{{url('/users/'. $bike->user->id . '/message')}}">Message the owner</a>
					@endif
				@else
					<a class="btn btn-social-action-small" href="#comment-section"> {{$bike->comments->count()}} <i class="fa fa-comment"></i></a>
					<a class="btn btn-social-action-small" href="#"> {{$bike->likes->count()}} <i class="fa fa-heart"></i></a>
				@endif-->
			</section>
		</section>
	</section>
	<section class="clearfix">
		<section class="bike-show-content-wrapper">
			<section class="bike-show-content-column column-sm">
				@if($bike->photos()->count())
					<section class="bike-show-image-gallery">
						<img class="bike-show-gallery-shown-image" src="" />
						<section class="bike-show-image-gallery-links clearfix">
							@foreach ($bike->photos as $photo)
								<section class="bike-show-image-gallery-link-wrapper">
									@if(Auth::User())
										@if (Auth::User()->id === $bike->user->id)
											<span>
												{!!Form::open(['url' => ['bikes/' . $bike->id . '/photos/' . $photo->id], 'method' => 'post','onsubmit' => 'return ConfirmDelete()']) !!}
													{{ csrf_field() }}
													<button type="submit" class="btn btn-small btn-default-inverse "><i class="fa fa-times"></i></button>
												{!! Form::close() !!}
											</span>
										@endif
									@endif
									<a class="bike-show-image-gallery-link" href="#"><img src="/{{$photo->thumbnail_path}}" /></a>
								</section>
							@endforeach
						</section>
					</section>
				@else
					<section class="bike-show-image-gallery">
						<img class="bike-show-gallery-shown-image" src="{!! asset('/images/placeholders/empty-bike.png') !!}" />
						@if(Auth::User())
							@if (Auth::User()->id === $bike->user->id)
								<p><a href="{{url('/bikes/'. $bike->id . '/addphotos')}}" class="sparkle">Foto toevoegen?</a> Het duurt maar enkele seconden.</p>
							@endif
						@endif
					</section>
				@endif
			</section>
			<section class="bike-show-content-column column-bg">
				<section class="bike-show-content-bike-details">
					<p class="title">{{ $bike->title }}</p>
					<p class="tussentitle">Type fiets</p>
					<p>{{ $bike->type }}</p>
					<p class="tussentitle">Omschrijving</p>
					<p>{{ $bike->description }}</p>
					<p class="tussentitle">Locatie</p>
					<!--<p>{{ $bike->street }} {{ $bike->streetnumber }}</p>-->
					<p>{{ $bike->zipcode }} {{ $bike->city }}</p>
					<div class="bike-price-hour">
					<!--<p class="tussentitle">Prijs / uur</p>
					<p>&euro; {{str_replace(".",",",$bike->price)}}</p>-->
					<!-- <p class="tussentitle">Prijs / dag</p>
					<p>&euro; 30,0</p> -->
					</div>
				</section>
				<!--@include('components.bikes.bikeschedule', ['bike' => $bike])-->
			</section>
		</section>
		<!--@include('components.bikes.bikecomments', ['bike' => $bike ])-->
	</section>
@stop

@section('scripts')
	<script>
		$(document).ready(function(){
			var firstimage = $('section.bike-show-image-gallery-links').find("img").attr('src');
			$("section.bike-show-image-gallery").find("img").first().attr('src', firstimage);
			$('.bike-show-image-gallery-link').click(function(e){
				var buttonClicked = $(this);
				var currentsrc = buttonClicked.find("img").attr('src');
				$("section.bike-show-image-gallery").find("img").first().attr('src', currentsrc);
				e.preventDefault();
			}); 
		});
	</script>
  @yield('filter.scripts')
@stop