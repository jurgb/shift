@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	<section class="container">
		<h1 class="formheader">Vul alle velden in om je fiets toe te voegen</h1>
		<p class="alert alert-info">Momenteel zijn wij op zoek naar personen die interesse hebben om Shift in de toekomst te gaan gebruiken. Je kan een account aanmaken en al fietsen toevoegen, het effectief huren of verhuren van fietsen is nog niet mogelijk.</p>
		@include('errors.formerrors')
		{!! Form::open(array('url' => '/bikes', 'method' => 'post','files' => true)) !!}
			{{ csrf_field() }}
			@include('bikes.partials.contentform', ['bike' => $bike])
			@include('bikes.partials.addphotos')
			<section class="form-button-wrapper align-button-right">
				{!! Form::submit('Fiets toevoegen!', ['class' => 'btn btn-default'])!!}
			</section>
		{!! Form::close() !!}
	</section>
@stop

@section('scripts')
@stop