@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	<section class="container">
		@include('errors.formerrors')
		{!! Form::open(array('url' => '/bikes/' . $bike->id, 'method' => 'put')) !!}
			{{ csrf_field() }}
			@include('bikes.partials.contentform', ['bike' => $bike])
			<section class="form-button-wrapper align-button-right">
				{!! Form::submit('Fiets updaten', ['class' => 'btn btn-default'])!!}
			</section>
		{!! Form::close() !!}
	</section>
@stop

@section('scripts')
@stop
	

	