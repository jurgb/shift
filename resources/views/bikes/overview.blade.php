@extends('layouts.default')
@section('navigation')

@stop

@section('content')
<div id="bike-overview" class="content bike-section">
	<div class="clearfix">
	<div class="headerimage">
		<section id="bike-filter" class="clearfix align-content-center">
			@include('bikes.partials.filter')
		</section>
	</div>
		<section class="action-links align-content-center">
			<a class="btn btn-default-inverse" href="{{url('/bikes/create')}}">Fiets toevoegen</a>
		</section>

		<section class="bikepreview flex" style="flex-wrap:wrap;">			
			@if($allbikes->count())
				@foreach($allbikes as $bike)
					@include('components.bikes.bikepreview', ['bike' => $bike])
				@endforeach
			@else
				<section class="container align-content-center">

					<div class="clearfix emptystate">
						<img src="{!! asset('/images/placeholders/emptystate-bikes.svg') !!}" alt="emptystate">
						<h2>Helaas zijn er geen fietsen gevonden...</h2>
					</div>

				</section>
			@endif
		</section>
	</div>
</div>
@stop

@section('scripts')
	@yield('filter.scripts')
	
@stop