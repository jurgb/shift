@extends('layouts.default')
@section('navigation')

@stop

@section('content')
<header class="front-header clearfix" style="background-image: url('images/main-image.jpg')">
	<div class="clearfix">
		<h1 class="call-to-shift">Rent <span>a <em>bike</em></span> <span class="sparkle">from a local.</span></h1>
		<div class="action-bar clearfix">
			<ul class="">
				<li><a href="#howto">Hoe werkt het?</a></li>
				<li><a href="#secure">Is het veilig?</a></li>
			</ul>
		</div>
	</div>
</header>
<div class="content">
	<div class="clearfix">
		<div id="intro">
			<section class="split shift-brand">
				<img src="images/logo-w.svg" alt="">
			</section>
			<section class="split storytext">
				<h2>Shift</h2>
				<p>Een deelplatform voor fietsen, gemaakt door en voor bike-lovers.</p>
				<p>Bij Shift willen we het gemakkelijker maken om in de toekomst overal een fiets te kunnen huren, want 
				<strong> @Shift, we love bikes!</strong> Waarom dan niet delen met like-minded, of bike-minded leden van onze community?</p>
				<p>Bijna iedereen heeft een fiets. Maar niet iedereen gebruikt zijn fiets op regelmatige basis. 
				Zie jij het zitten om je ongebruikte fiets een nieuw leven te geven? </p>
				<p class=""><strong>Are you ready to Shift?</strong></p>
			</section>
		</div>


		<div id="howto">
			<h2>Hoe werkt het? <span class="icon fa fa-bicycle"></span></h2>
			<div class="howto huren">
				<section class="left split">
					<h3>Ik wil een fiets <br><strong>huren...</strong><i class="fa fa-arrow-right"></i></h3>
				</section>
				<section class="right split">					
					<ul>
						<li><p><i class="fa fa-check sparkle"></i>Word gratis lid van onze bike-minded community.</p></li>
						<li><p><i class="fa fa-check sparkle"></i>Huur een fiets waar je hem nodig hebt en laat de auto thuis.</p></li>
						<li><p><i class="fa fa-check sparkle"></i>Je betaalt enkel voor de tijd dat je de fiets nodig hebt.</p></li>
					</ul>
				</section>
			</div>
			<div class="howto verhuren">
				<section class="left split">
					<ul>
						<li><p><i class="fa fa-check sparkle"></i>Plaats je fiets gratis op ons platform.</p></li>
						<li><p><i class="fa fa-check sparkle"></i>Geef je ongebruikte fiets een nieuw leven.</p></li>
						<li><p><i class="fa fa-check sparkle"></i>Verdien iets aan je fiets die anders stilstaat.</p></li>
					</ul>
				</section>
				<section class="right split">
					<h3>Ik wil mijn fiets <br><i class="fa fa-arrow-left"></i><strong>verhuren...</strong></h3>
				</section>
			</div>
		</div>

		<div id="community">
			<img src="/images/logo-w.svg" alt="logo white">
			<h2><strong>Shift</strong>, dé bike-minded community.</h2>
			<a id="btnJoin" class="" href="{{url('/register')}}">Lid worden</a>
			<p class="text-center" style="color:#fff;font-size:12px;margin-top:-20px;">Het is gratis!</p>
		</div>

		<div id="secure">
		<section class="split left">
			<h2><strong>Veiligheid</strong> is ook onze grootste zorg.</h2>
			<p class="text-center">Hoe pakken we dat aan bij <strong>Shift</strong>?<i class="fa fa-arrow-right"></i></p>
		</section>
		<section class="split right">
			<p>Bij <strong>Shift</strong> proberen we het delen van fietsen zo veilig mogelijk te maken. Dit proberen we op verschillende manieren:</p>

			<ul class="safety-measures">
				<li><span class="circle">1</span>
					<p>Op ons platform werken we met sociale controle en ratings. Onze leden kunnen elkaar beoordelen en zo een vertrouwensband creëeren. Wij vragen onze leden ook om zich zo goed mogelijk te verifiëren d.m.v. sociale media te koppelen met hun profiel op Shift.</p>
				</li>
				<li><span class="circle">2</span>
					<p>Wij zijn ook voortdurend op zoek naar partnerships met fietsinnovatieve bedrijven of verzekeringskantoren die in de toekomst kunnen bijdragen tot de veiligheid van uw fiets.</p>
				</li>
				<li><span class="circle">3</span>
					<p>Ten slotte vragen wij onze leden ook om zelf een oogje in het zeil te houden en bij twijfel zelf de nodige stappen te nemen om de veiligheid van uw fiets te verzekeren.</p>
				</li>
			</ul>
		</section>
		</div>

		<div id="shiftwithus">
		<h2><span class="sparkle">#</span>shiftwithus</h2>
			<div id="newsletter">
				<p>Wij gaan binnenkort online. Wil jij er als eerste bij zijn? Schrijf je in op onze nieuwsbrief en we houden je op de hoogte.</p>

				<!-- Begin MailChimp Signup Form -->
			        <div class="mc_embed_signup clearfix">
			          <form action="/tpl/subscribe.php" id="invite" method="POST">
		            	{{ csrf_field() }}
			            <input type="email" placeholder="your@email.com" name="email" id="address" data-validate="validate(required, email)"/>
			            <input type="submit" value="Inschrijven" id="btnMcSubmit">
			          </form>
			          <span id="result"></span>
			        </div>
			    <!--End mc_embed_signup-->

			</div>

			<div id="social">
			<p>Of fiets met ons mee op Strava</p>
				<iframe id="strava" allowtransparency='true' scrolling='no' src='https://www.strava.com/clubs/goshift/latest-rides/ad91792f952adaf5eae8e28f16f4b0a1e474ddf6?show_rides=false'></iframe>
			</div>
		</div>

	</div>
</div>
@stop

@section('scripts')

@stop
