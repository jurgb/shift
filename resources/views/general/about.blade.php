@extends('layouts.default')
@section('navigation')

@stop

@section('content')
<header class="front-header clearfix" style="background-image: url('images/main-image.jpg')">
	<div class="clearfix">
		<h1 class="call-to-shift">Hey, we <span>are <em>Shift</em></span> <span class="sparkle">nice to meet you.</span></h1>
		<div class="action-bar clearfix">
			<ul class="">
				<li><a href="#howto">Wat is ons doel?</a></li>
				<li><a href="#secure">Wie zijn wij?</a></li>
			</ul>
		</div>
	</div>
</header>
<div class="content">
	<div class="clearfix">
		<div id="intro">
			<section class="split shift-brand">
				<img src="images/logo-w.svg" alt="">
			</section>
			<section class="split storytext">
				<h2>Shift</h2>
				<p>Een deelplatform voor fietsen, gemaakt door en voor bike-lovers.</p>
				<p>Bij Shift willen we het gemakkelijker maken om in de toekomst overal een fiets te kunnen huren, want 
				<strong> @Shift, we love bikes!</strong> Waarom dan niet delen met like-minded, of bike-minded leden van onze community?</p>
				<p>Bijna iedereen heeft een fiets. Maar niet iedereen gebruikt zijn fiets op regelmatige basis. 
				Zie jij het zitten om je ongebruikte fiets een nieuw leven te geven? </p>
				<p class=""><strong>Are you ready to Shift?</strong></p>
			</section>
		</div>


		<div id="howto">
			<h2>Wat is ons doel? <span class="icon fa fa-bicycle"></span></h2>
			<div class="howto huren">
				<section class="left split">
					<h3>Wij verhuren <br><strong>fietsen...</strong><i class="fa fa-arrow-right"></i></h3>
				</section>
				<section class="right split">					
					<ul>
						<li><p><i class="fa fa-check sparkle"></i>Iedereen die tijdelijk een fiets nodig heeft, kan via Shift een leuke fiets huren.</p></li>
						<li><p><i class="fa fa-check sparkle"></i>Iedereen die thuis en fiets heeft staan die hij niet gebruikt, kan hem verhuren via Shift.</p></li>
					</ul>
				</section>
			</div>
			<div class="howto verhuren">
				<section class="left split">
					<ul>
						<li><p><i class="fa fa-check sparkle"></i>Shift wil mensen met dezelfde interesse samenbrengen. Er is niets leuker dan persoonlijke ervaringen te delen</p></li>
						<li><p><i class="fa fa-check sparkle"></i>Word ook lid en deel jouw persoonlijke ervaringen met ons en onze gebruikers. </p></li>
					</ul>
				</section>
				<section class="right split">
					<h3>Wij bouwen een <br><i class="fa fa-arrow-left"></i><strong>community...</strong></h3>
				</section>
			</div>
		</div>

		<div id="community">
			<img src="/images/logo-w.svg" alt="logo white">
			<h2><strong>Shift</strong>, dé bike-minded community.</h2>
			<a id="btnJoin" class="" href="{{url('/register')}}">Lid worden</a>
			<p class="text-center" style="color:#fff;font-size:12px;margin-top:-20px;">Het is gratis!</p>
		</div>

		<div id="secure">
		<section class="split left">
			<h2>Ontdek ons <strong>team</strong></h2>
			<p class="text-center">Wij zijn <a href="http://www.weareimd.be">3 #WeAreIMD studenten</a> met een passie voor de fiets<i class="fa fa-arrow-right"></i></p>
		</section>
		<section class="split right">
			<ul class="safety-measures">
				<li>
					<h2>Jens Ivens</h2>
					<p class="text-center">Founder & CEO</p>
					<p>Hallo, ik ben Jens. Ik ben de ploegleider van Shift (om het in fietstermen uit te drukken). Ik probeer ervoor te zorgen dat onze renners doen wat er van hun verwacht wordt en zo proberen we geregeld eens een race te winnen.</p>
				</li>
				<li>
					<h2>Yannick Nijs</h2>
					<p class="text-center">Co-Founder & Front-End Developer</p>
					<p>Hallo, ik ben Yannick. Ik ontwerp de truitjes en de fietsen. Mijn grootste zorg is dat alles er goed moet uitzien en als dat niet zo is, dan doe ik enkele aanpassingen om Shift mooier er leuker te maken.</p>
				</li>
				<li>
					<h2>Jurgen Barbier</h2>
					<p class="text-center">Back-end Developer</p>
					<p>Hallo, ik ben Jurgen. Ik ben de mechanieker van Shift. Ik zorg er persoonlijk voor dat alles werkt, en dat jullie gebruik kunnen maken van ons platform.</p>
				</li>
				
			</ul>
		</section>
		</div>

		<div id="shiftwithus">
		<h2><span class="sparkle">#</span>shiftwithus</h2>
			<div id="newsletter">
				<p>Wij gaan binnenkort online. Wil jij er als eerste bij zijn? Schrijf je in op onze nieuwsbrief en we houden je op de hoogte.</p>

				<!-- Begin MailChimp Signup Form -->
			        <div class="mc_embed_signup">
			          <form action="/tpl/subscribe.php" id="invite" method="POST">
			            <input type="email" placeholder="your@email.com" name="email" id="address" data-validate="validate(required, email)"/>
			            <input type="submit" value="Inschrijven" id="btnMcSubmit">
			          </form>
			          <span id="result"></span>
			        </div>
			    <!--End mc_embed_signup-->

			</div>

			<div id="social">
			<p>Of fiets met ons mee op Strava</p>
				<iframe id="strava" allowtransparency='true' scrolling='no' src='https://www.strava.com/clubs/goshift/latest-rides/ad91792f952adaf5eae8e28f16f4b0a1e474ddf6?show_rides=false'></iframe>
			</div>
		</div>

	</div>
</div>
@stop

@section('scripts')

@stop
