@extends('layouts.default')

@section('content')
<section class="authenticate login">
	<div class="split left">
		<img src="/images/logo-w.svg" alt="logo shift">
	</div>
	<div class="split right">
		<section class="form">
		<h1>Inloggen</h1>
		<p>Log in, beheer je profiel, voeg fietsen toe en <em>start to Shift</em></p>
		<p class="alert alert-info">Momenteel zijn wij op zoek naar personen die interesse hebben om Shift in de toekomst te gaan gebruiken. Je kan inloggen en fietsen toevoegen, het effectief huren of verhuren van fietsen is nog niet mogelijk. </p>

		@include('errors.formerrors')
	
		{!! Form::open(array('url' => '/login', 'method' => 'POST'))!!}
  		{{ csrf_field() }}
	  	{!! Form::email('email', null, ['class' => '', 'placeholder' => 'Emailadres']) !!}
	    {!! Form::password('password', ['class' => '', 'placeholder' => 'Wachtwoord'])!!}
  	    {!! Form::label('remember', 'Remember me') !!}
	    {!! Form::checkbox('remember', 'remember', true) !!}
	    <div class="password-reset"><a href="{{ url('/password/reset') }}">wachtwoord vergeten?</a></div>
			{!! Form::submit('Inloggen', ['class' => 'btn btn-default-inverse'])!!}
	  {!! Form::close() !!}
		<p>Heb je nog geen account? <br> Geen probleem! Maak er <a href="/register">hier</a> eentje aan.</p>
		</section>
	</div>
</section>

@stop