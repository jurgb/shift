@extends('layouts.default')
@section('content')
<section class="authenticate resetpassword">
	<div class="split left">
		<img src="/images/logo-w.svg" alt="logo shift">
	</div>
	<div class="split right">
	<section class="form">
		<h1>Reset Password</h1>
		<p>Hier kan je je wachtwoord opnieuw instellen, vul onderstaande velden in.</p>
		
		@include('errors.formerrors')
		    
		{!! Form::open(array('url' => '/password/reset', 'method' => 'POST'))!!}
			{{ csrf_field() }}
			{!! Form::hidden('token', $token) !!}
			{!! Form::email('email', null, ['id' => 'email', 'class' => '', 'placeholder' => 'Email']) !!}
			{!! Form::password('password', ['id' => 'password', 'class' => '', 'placeholder' => 'Paswoord']) !!}
			{!! Form::password('password_confirmation', ['id' => 'password-confirm', 'class' => '', 'placeholder' => 'Confirm paswoord']) !!}
			{!! Form::submit('Stel opnieuw in', ['class' => 'btn btn-default-inverse'])!!}
		{!! Form::close() !!}
	</section>
	</div>
	</section>
@stop