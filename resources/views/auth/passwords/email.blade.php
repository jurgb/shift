@extends('layouts.default')
@section('content')
<section class="authenticate resetpassword">
	<div class="split left">
		<img src="/images/logo-w.svg" alt="logo shift">
	</div>
	<div class="split right">
	<section class="form">
		<h1>Wachtwoord vergeten? Geen probleem!</h1>
		<p>Ben je je wachtwoord vergeten? Geen paniek, wij helpen je verder. Vul het emailadres van jouw account in en je ontvangt van ons een mailtje met een link om je wachtwoord te resetten.</p>
		
		@include('errors.formerrors')
		    
		{!! Form::open(array('url' => '/password/email', 'method' => 'POST'))!!}
			{{ csrf_field() }}
			{!! Form::email('email', null, ['class' => '', 'placeholder' => 'Emailadres van jouw account']) !!}
			{!! Form::submit('Vraag nieuw wachtwoord aan', ['class' => 'btn btn-default-inverse'])!!}
		{!! Form::close() !!}

		<p>Wachtwoord toch niet vergeten? Probeer dan nog eens <a href="{{url('/login')}}">in te loggen</a>.</p>
	</section>
	</div>
	</section>
@stop