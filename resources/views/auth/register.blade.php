@extends('layouts.default')
@section('content')
<section class="authenticate register">
	<div class="split left">
		<img src="/images/logo-w.svg" alt="logo shift">
	</div>
	<div class="split right">
	<section class="form">
		<h1>Account aanmaken</h1>
		<p>Ben je klaar om de wereld mee te veranderen? Maak snel je account aan en vind een fiets in jouw buurt of verhuur jouw fiets aan iemand van onze community.</p>
		<p class="alert alert-info">Momenteel zijn wij op zoek naar personen die interesse hebben om Shift in de toekomst te gaan gebruiken. Je kan een account aanmaken en al fietsen toevoegen, het effectief huren of verhuren van fietsen is nog niet mogelijk. </p>
		@include('errors.formerrors')
		    
		{!! Form::open(array('url' => '/register', 'method' => 'POST'))!!}
			{{ csrf_field() }}
			{!! Form::text('firstname', null, ['class' => '', 'placeholder' => 'Voornaam']) !!}
			{!! Form::text('lastname', null, ['class' => '', 'placeholder' => 'Achternaam']) !!}
			{!! Form::email('email', null, ['class' => '', 'placeholder' => 'Emailadres']) !!}
			{!! Form::password('password', ['class' => '', 'placeholder' => 'Wachtwoord']) !!}
			{!! Form::submit('Aanmelden', ['class' => 'btn btn-default-inverse'])!!}
		{!! Form::close() !!}

		<p>Heb je al een account? <br> Dan kan je <a href="/login">hier inloggen</a>.</p>
	</section>
	</div>
	</section>
@stop