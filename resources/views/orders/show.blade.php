@extends('layouts.default')
@section('navigation')

@stop

@section('content')
<div id="order-step-2" class="content">
	<section class="branding split">
		<h2>Fiets reserveren</h2>
		<p>Stap <span>2</span> van <span>2</span></p>
	</section>
	<div class="order-wrapper split">
		<h1 class="formheader">Reservatie van: <em>{{ $order->bike->title }}</em></h1>
		@if (Session::has('flash_notification.message'))
    	<p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }} alert-top alert-second">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
                </p>
    @endif
		<section class="order-overview">
			@if($order->status != 'cancelled')
			<div id="order-pending" class="clearfix">
			<section class="order-date-box">
				<h3><i class="fa fa-arrow-circle-o-up"></i> Pick up</h3>
				<p>Op {{ date('j F Y', strtotime($order->pickup)) }}</p>
				<p>Om {{ date('H\h i', strtotime($order->pickup)) }}</p>
			</section>
			<section class="order-date-box">
				<h3><i class="fa fa-arrow-circle-o-down"></i> Drop off</h3>
				<p>Op {{ date('j F Y', strtotime($order->dropoff)) }}</p>
				<p>Om {{ date('H\h i', strtotime($order->dropoff)) }}</p>
			</section>
			</div>
			<section class="sum-up-price">
				<p>Prijs: &euro;{{ $order->price }}</p>
			</section>
				@if($order->status == 'sent')
					@if($order->seller_id == Auth::User()->id)
						<a class="btn btn-small btn-default" href="{{url('/order/'. $order->id .'/accept')}}"><i class="fa fa-check"></i> Accepteer</a>
				<a class="btn btn-small btn-default" href="{{url('/order/'. $order->id .'/decline')}}"><i class="fa fa-times"></i> Weiger</a>
					@else
						<p class="order_status"><i class= "fa fa-spinner"></i> wachten op bevestiging</p>
					@endif
				@elseif($order->status == 'accepted')
					@if($order->seller_id == Auth::User()->id)
						<p class="order_status"><i class="fa fa-info-circle"></i> Je hebt deze resevatie geaccepteerd! {{$order->user->firstname}} moet enkel nog betalen.</p>
					@else
					{!! Form::open(array('url' => '/order/' . $order->id , 'method' => 'POST')) !!}        
						<script
							src="https://checkout.stripe.com/checkout.js" class="stripe-button btn-small"
							id="stripe"
							data-key="pk_test_ywdllGSOf0LTmL8PuxRIp4dI"
							data-name="Enjoy your ride, {{Auth::user()->firstname}}"
							data-description="Please fill in this for to finish your order"
							data-currency="eur"
							data-amount="{{$order->price * 100}}"
							data-locale="auto">
						</script>
					{!! Form::close() !!}
<!-- 						<a class="btn btn-small btn-default" href="">Betaal</a>
						<p class="order_status"><i class="fa fa-info-circle"></i> Betalen is nog niet mogelijk in deze fase van de ontwikkeling</p> -->
					@endif
				@elseif($order->status == 'declined')
					<p class="order_status"><i class="fa fa-times-circle"></i> De reservatie is geweigerd</p>
				@elseif($order->status == 'paid')
					<p class="order_status"><i class="fa fa-info-circle"></i> Alles is betaald en in orde! Veel plezier tijdens de fietstocht! </p>
				@else
					<p class="order_status"><i class="fa fa-wrench"></i> Er is iets mis, gelieve ons te contacteren!</p>
				@endif
				{!!Form::open(['url' => '/order/'. $order->id, 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) !!}
					{{ csrf_field() }}
					<button type="submit" class="btn btn-small btn-default-inverse">Order annuleren</button>
				{!! Form::close() !!}
			@else
				<p class="order_status"><i class="fa fa-info-circle fa-4x"></i></p>
				<p class="order_status">Dit order is geannuleerd!</p>
				<a class="btn btn-default" href="{{url('/bikes')}}">Terug naar fietsen</a>
			@endif
		</section>
	</div>
</div>
@stop

@section('scripts')
	
@stop