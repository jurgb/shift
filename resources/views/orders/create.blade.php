@extends('layouts.default')
@section('navigation')

@stop

@section('content')
<?php
	$timescollection = [""=>"selecteer een tijdstip","00:00" => "00:00","01:00" => "01:00","02:00" => "02:00","03:00" => "03:00","04:00" => "04:00","05:00" => "05:00","06:00" => "06:00","07:00" => "07:00","08:00" => "08:00","09:00" => "09:00","10:00" => "10:00","11:00" => "11:00","12:00" => "12:00","13:00" => "13:00","14:00" => "14:00","15:00" => "15:00","16:00" => "16:00","17:00" => "17:00","18:00" => "18:00","19:00" => "19:00","20:00" => "20:00","21:00" => "21:00","22:00" => "22:00","23:00" => "23:00"]
?>
<section id="order-step-1" class="content">
	<section class="branding split">
		<h2>Fiets reserveren</h2>
		<p>Stap <span>1</span> van <span>2</span></p>
	</section>
	<section class="order-wrapper split">
		<h1 class="formheader">Reservatie van: <em>{{ $bike->title }}</em></h1>
		<section class="order-form">
			@include('errors.formerrors')
			{!! Form::open(array('url' => '/bikes/'. $bike->id .'/order', 'method' => 'POST'))!!}
				{{ csrf_field() }}
			<section class="order-date-box">
				<h3><i class="fa fa-arrow-circle-o-up"></i> Pick up datum</h3>
				<section class="form-labeled-field-full">
					{!! Form::label('pickup_date', 'Datum', ['class' => '']) !!}
					{!! Form::text('pickup_date', null, ['class' => 'input_bottom_border datepicker', 'placeholder' => 'Selecteer datum']) !!}
				</section>
				<section class="flex" style="width:100%">
					{!! Form::label('pickup_start_time', 'Start time', ['class' => '']) !!}
					<section class="flex">
						<section style="width:45%;">
							{!! Form::number('pickup_start_time_hour', null, ['class' => '', 'min' => "0", 'max' => '23']) !!}
						</section>
						<section style="width:10%">
							{!! Form::label('pickup_start_time_hour', '&nbsp;:', ['class' => '', 'style' => '']) !!}
						</section>
						<section style="width:45%;">
						{!! Form::number('pickup_start_time_min', null, ['class' => '', 'min' => "0", 'max' => '59']) !!}
						</section>
					</section>
				</section>
			</section>
			
			<section class="order-date-box">
				<h3><i class="fa fa-arrow-circle-o-down"></i> Drop off datum</h3>
				<section class="form-labeled-field-full">
					{!! Form::label('dropoff_date', 'Datum', ['class' => '']) !!}
					{!! Form::text('dropoff_date', null, ['class' => 'input_bottom_border datepicker', 'placeholder' => 'Selecteer datum']) !!}
				</section>
				<section class="flex" style="width:100%">
					{!! Form::label('dropoff_start_time', 'Start time', ['class' => '']) !!}
					<section class="flex">
						<section style="width:45%;">
							{!! Form::number('dropoff_start_time_hour', null, ['class' => '', 'min' => "0", 'max' => '23']) !!}
						</section>
						<section style="width:10%">
							{!! Form::label('dropoff_start_time_hour', '&nbsp;:', ['class' => '', 'style' => '']) !!}
						</section>
						<section style="width:45%;">
						{!! Form::number('dropoff_start_time_min', null, ['class' => '', 'min' => "0", 'max' => '59']) !!}
						</section>
					</section>
				</section>
			</section>
			<section class="form-button-wrapper align-button-right">
				{!! Form::submit('Plaats bestelling', ['class' => 'btn btn-default'])!!}
			</section>
			{!! Form::close() !!}
		</section>
	</section>
</section>
@stop

@section('scripts')
  <script src="/js/libs/jqueryui.js"></script>
  <script src="/js/libs/moment.js"></script>
  <script src="/js/libs/pickaday.js"></script>
  <script src="/js/libs/pickaday-jquery.js"></script>
  <script>
		userschedule = <?php echo json_encode($bike->schedule->first()); ?>;
  	$('.datepicker').pikaday({
			firstDay: 1,
			minDate: new Date(),
    	disableDayFn: function(date){
				if(date.getDay() === 1 && userschedule.monday == 0)
				{
					return date.getDay() === 1;
				}
				if(date.getDay() === 2 && userschedule.tuesday == 0)
				{
					return date.getDay() === 2;
				}
				if(date.getDay() === 3 && userschedule.wednesday == 0)
				{
					return date.getDay() === 3;
				}
				if(date.getDay() === 4 && userschedule.thursday == 0)
				{
					return date.getDay() === 4;
				}
				if(date.getDay() === 5 && userschedule.friday == 0)
				{
					return date.getDay() === 5;
				}
				if(date.getDay() === 6 && userschedule.saturday == 0)
				{
					return date.getDay() === 6;
				}
				if(date.getDay() === 0 && userschedule.sunday == 0)
				{
					return date.getDay() === 0;
				}
      },
      onSelect: function() {
      	console.log(this.getMoment().format('Do MMMM YYYY'));
      }
    });
  </script>
@stop