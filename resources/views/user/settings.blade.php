@extends('layouts.default')
@section('navigation')

@stop

@section('content')
		@include("user.partials.dashboard_menu", ['active' => 'settings'])
		<section class="user-page clearfix">
			@include("user.partials.dashboard_side_menu")
			<section class="dashboard_content">
				<section id="dashboard_notification_settings">
					<section class="dashboard_title_area">Notifications settings</section>
					<section class="dashboard_content_item">
						<p class="dashboard_emptystate_message">Hier komen de instellingen voor meldingen</p>
					</section>
				</section>
				<section id="dashboard_payment_settings">
					<section class="dashboard_title_area">Payment settings</section>
					<section class="dashboard_content_item">
						<p class="dashboard_emptystate_message">Hier komen de instellingen voor betalingen</p>
					</section>
				</section>
				<section id="dashboard_privacy_settings">
					<section class="dashboard_title_area">Privacy settings</section>
					<section class="dashboard_content_item">
						<p class="dashboard_emptystate_message">Hier komen de instellingen voor privacy</p>
					</section>
				</section>
				<section id="dashboard_verification_settings">
					<section class="dashboard_title_area">Verification settings</section>
					<section class="dashboard_content_item">
						<section class="dashboard_verification_item dashboard_verification_email">
							<i class="fa fa-at fa-4x"></i>
							<section class="dashboard_verification_item_status_wrapper">
								@if(Auth::user()->email_verification == 1)
									<section class="verification_check"><i class="fa fa-check-circle"></i> Verified</section>
								@else
									<a class="btn btn-default verification_button" href="{{url('/users/resendverifyemail')}}">Verify email</a>
								@endif
							</section>
						</section>
						<section class="dashboard_verification_item dashboard_verification_facebook">
							<i class="fa fa-facebook-square fa-4x"></i>
							<section class="dashboard_verification_item_status_wrapper">
								<section class="verification_check" @if(Auth::user()->facebook_verification != 1) style="display:none;" @endif><i class="fa fa-check-circle"></i> Verified</section>
								<section class="verification_button">
									<div id="fb_verification_status" class="dashboard_emptystate_message"></div>
									<div class="fb-login-button" data-size="xlarge" data-show-faces="false" data-auto-logout-link="false" onlogin="checkLoginState();"></div>
								</section>
							</section>
						</section>
					</section>
				</section>
				<section id="dashboard_account_settings">
					<section class="dashboard_title_area">Account settings</section>
					<section class="dashboard_content_item">
						<p class="dashboard_emptystate_message">Hier komen de instellingen voor account</p>
					</section>
				</section>
			</section>
		</section>
@stop

@section('scripts')
<script>
  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('fb_verification_status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('fb_verification_status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  function testAPI() {
    console.log('Fetching information from facebook API.... ');
    FB.api('/me', function(response) {
			document.getElementById('fb_verification_status').innerHTML = 'Successful login for: ' + response.name;
			var response = $.ajax({
				url: '/users/addfbverification',
				data: {'facebook_verification':1,'_token':$('meta[name="_token"]').attr('content')},
				type: "post",	
				dataType: "json",
				success: function(data){
					if (data.status == "succes"){
						console.log('Facebook has been added to your verifications');
						$('.dashboard_verification_facebook').find('.verification_button').fadeOut(function(){
							$('.dashboard_verification_facebook').find('.verification_check').fadeIn(400);
						});
					}
				},
				error: function(errres){
					alert("Er ging its mis, herlaad de pagina en probeer opnieuw! Onze excuses voor dit ongemak.");
				}
			});
    });
  }
</script>
	@yield('completerscripts')
@stop
