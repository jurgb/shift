@extends('layouts.default')
@section('navigation')

@stop

@section('content')
		@include("user.partials.dashboard_menu", ['active' => 'reservations'])
		<section class="user-page clearfix">
			<section>
				<h1 class="reservation_header">Aankopen</h1>
				@if($purchases->count())
					<section class="reservation_headers flex">
						<section class="reservation_bike">
							Fietsnaam
						</section>
						<section class="reservation_status">
							Status
						</section>
						<section class="reservation_pickup">
							<i class="fa fa-arrow-circle-o-up"></i> Pick up
						</section>
						<section class="reservation_dropoff">
							<i class="fa fa-arrow-circle-o-down"></i> Drop off
						</section>
						<section class="reservation_price">
							Prijs
						</section>
						<section class="reservation_actions">
							Actions
						</section>
					</section>
					@foreach($purchases as $purchase)
						@include("user.partials.reservation_preview", ['item' => $purchase])
					@endforeach
				@else
					<p class="dashboard_emptystate_message">Geen aankopen om weer te geven.</p>
				@endif
			</section>
			<section>
				<h1 class="reservation_header">Verkopen</h1>
				@if($sales->count())
					<section class="reservation_headers flex">
						<section class="reservation_bike">
							Fietsnaam
						</section>
						<section class="reservation_status">
							Status
						</section>
						<section class="reservation_pickup">
							<i class="fa fa-arrow-circle-o-up"></i> Pick up
						</section>
						<section class="reservation_dropoff">
							<i class="fa fa-arrow-circle-o-down"></i> Drop off
						</section>
						<section class="reservation_price">
							Prijs
						</section>
						<section class="reservation_actions">
							Actions
						</section>
					</section>
					@foreach($sales as $sale)
						@include("user.partials.reservation_preview", ['item' => $sale])
					@endforeach
				@else
					<p class="dashboard_emptystate_message">Geen verkopen om weer te geven.</p>
				@endif
			</section>
		</section>
@stop

@section('scripts')
	@yield('completerscripts')
@stop