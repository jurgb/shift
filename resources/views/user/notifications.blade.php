@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	@include("user.partials.dashboard_menu", ['active' => 'notifications'])
	<section class="user-page clearfix">
		@include("general.partials.alerts")

		@include("user.partials.dashboard_side_menu")
		<section class="dashboard_content">
			<section class="dashboard_notifications">
				<section class="dashboard_title_area">Notifications <span class="notification-counter" @if(Auth::User()->newNotifications()->count() == 0) style="display:none;" @endif >{{Auth::User()->newNotifications()->count()}}</span></section>
				<section class="dashboard_content_item">
					@if(count(Auth::User()->newNotifications()))
						@foreach(Auth::User()->newNotifications() as $notification)
							@include('user.partials.dashboard_dynamic_notification', ['notification' => $notification])
						@endforeach
					@else
						<p class="dashboard_emptystate_message">Er zijn geen notificaties om weer te geven!</p>
					@endif
				</section>
			</section>
		</section>
	</section>
@stop

@section('scripts')

	<script>
		$(document).ready(function(){
			$('.read_notification_button').click(function(e){
				$(this).closest('.dashboard_notification_item').slideUp(400);
				headsection = $(this).parent();
				var response = $.ajax({
					url: '/notification/read',
					data: {'notification_id':headsection.find('input[name=notification_id]').val(),'_token':$('meta[name="_token"]').attr('content')},
					type: "post",	
					dataType: "json",
					success: function(data){
						if (data.status == "succes"){
							$('.notification-counter').each(function(){
								if(data.notificationscount == 0)
								{
									$(this).hide();
								} else {
									$(this).text(data.notificationscount);
								}
							});
							if(data.notificationscount == 0)
							{
								$('.dashboard_content_item').append("<p style='display:none;' class='dashboard_emptystate_message'>Er zijn geen notificaties om weer te geven!</p>");
								$('.dashboard_emptystate_message').each(function(){
									$(this).fadeIn(400);
								});
							}
						}
					},
					error: function(errres){
						alert("Er ging its mis, herlaad de pagina en probeer opnieuw! Onze excuses voor dit ongemak.");
					}
				});
				e.preventDefault();	
			});
		});
	</script>
	@yield('completerscripts')
@stop
