@extends('layouts.default')
@section('navigation')

@stop

@section('content')
		@include("user.partials.dashboard_menu", ['active' => 'messages'])
		<section class="user-page clearfix">

		@include("general.partials.alerts")

		@include("user.partials.dashboard_side_menu")

		<section class="messages_content clearfix">
			<div class="header-image clearfix" style="background:url({!! asset('/images/brand.jpg') !!});">
					<h1>Berichten</h1>
					<p>De Shift community wacht op jou!</p>
					<p>Hier kan je jouw berichten vinden, het is altijd leuk om je ervaringen te delen met andere <em>Shifters</em>! @if($conversations != null) Klik op een bericht om de conversatie te openen @endif</p>
			</div>
		@if($conversations != null)
		<div class="list-group">
			@if($conversations->count())
				@foreach($conversations as $conversation)
							<a class="list-group-item" href="{{url('/users/'. $conversation->participants()->where('user_id', '!=', Auth::User()->id)->first()->user_id) . '/message' }}">
								@if($conversation->participants()->where('user_id', '!=', Auth::User()->id)->first()->user->image)
									<img src="/{{$conversation->participants()->where('user_id', '!=', Auth::User()->id)->first()->user->image->thumbnail_path}}" alt="" class="img-circle">
								@else
									<img src="{!! asset('/images/placeholders/user-default.png') !!}" alt="" class="img-circle">
								@endif
							    <p class="right">{{$conversation->messages->last()->updated_at->diffForHumans()}}</p>

							    <p class="list-group-item-text name">{{$conversation->participants()->where('user_id', '!=', Auth::User()->id)->first()->user->firstname}} {{$conversation->participants()->where('user_id', '!=', Auth::User()->id)->first()->user->lastname}}</p>

								<p class="list-group-item-text content">{{$conversation->messages->last()->content}}</p>
							</a>
				@endforeach
			@else
				<div class="list-group">
						<h4 class="list-group-item-heading">Je hebt nog geen conversaties met anderen</h4>
						<p class="list-group-item-text">Ga naar het profiel van een andere shifter en start een conversatie.</p>
				</div>
		@endif
			</ul>

		@else
				<div class="list-group">
						<h4 class="list-group-item-heading">Je hebt nog geen conversaties met anderen</h4>
						<p class="list-group-item-text">Ga naar het profiel van een andere shifter en start een conversatie.</p>
				</div>
		@endif
		</section>
@stop

@section('scripts')
	@yield('completerscripts')
	<script>
        document.getElementById("profilepicture").onchange = function() {
            document.getElementById("profilepicchanger").submit();
        };
    </script>
@stop