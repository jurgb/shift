@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	<section id="public-profile" class="clearfix">
		<div class="headerimage"></div>

		<div class="profile-show-header">
			<div class="profile-pic">
				@if($user->image)
				<img src="/{{$user->image->thumbnail_path}}" alt="">
				@else
				<img src="{!! asset('/images/placeholders/user-default.png') !!}" alt="">
				@endif
			</div>
			<div class="profile-name">
				<h1>{{ $user->firstname }} {{ $user->lastname }}</h1>
				<h2>lid sinds {{ date('j F Y', strtotime($user->created_at)) }}</h2>
			</div>
			<div class="message-user pull-right">
				@if($user->id != Auth::User()->id)
					<a class="btn btn-small btn-default-inverse" href="{{url('/users/'. $user->id . '/message')}}">Stuur een bericht</a>
				@endif
			</div>
		</div>

		<section id="user-profile-bikes">
			<h2>Alle fietsen van {{ $user->firstname }} {{ $user->lastname }} ({{ $user->bikes->count() }})</h2>
			<section class="bikepreview flex" style="flex-wrap:wrap;">
				@if($user->bikes->count())
					@foreach($user->bikes as $bike)
						@include('components.bikes.bikepreview', ['bike' => $bike])
					@endforeach
				@else
					<section class="container align-content-center">
						<p> Deze gebruiker heeft nog geen fiets toegevoegd</p>
					</section>
				@endif
			</section>
		</section>

		@include("user.partials.profile_side_menu", ['user' => $user])

		<section class="profile_content">
			<section>
				<h2>Recensies (0)</h2>
				<section class="flex" style="flex-wrap:wrap;">
					@if($user->reviews)
						
					@else
						<section class="container align-content-center">
							<p>Er zijn nog geen recensies geschreven over deze persoon</p>
						</section>
					@endif
				</section>
			</section>
		</section>
	</section>
@stop

@section('scripts')

@stop