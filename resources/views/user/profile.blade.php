@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	@include("user.partials.dashboard_menu", ['active' => 'profile'])

	<section class="user-page clearfix">
		@include("user.partials.dashboard_side_menu")
		<section class="dashboard_content">
			@include("general.partials.alerts")

			<section class="dashboard_title_area">Personal information</section>
			<section id="edit-profile" class="dashboard_content_item">
				{!! Form::open(array('url' => '/users/update', 'method' => 'POST'))!!}
					{{ csrf_field() }}
					<section class="clearfix form-labeled-field-full">
						{!! Form::label('firstname', 'Voornaam', ['class' => '']) !!}
						{!! Form::text('firstname', Auth::User()->firstname , ['class' => 'input_bottom_border', 'placeholder' => 'Voornaam']) !!}
					</section>
					<section class="clearfix form-labeled-field-full">
						{!! Form::label('lastname', 'Achternaam', ['class' => '']) !!}
						{!! Form::text('lastname', Auth::User()->lastname , ['class' => 'input_bottom_border', 'placeholder' => 'Achternaam']) !!}
					</section>
					<section class="clearfix form-labeled-field-full">
						{!! Form::label('street', 'Straat', ['class' => '']) !!}
						{!! Form::text('street', Auth::User()->street , ['class' => 'input_bottom_border', 'placeholder' => 'Straatnaam']) !!}
						<span class="form-field-notes"> Dit veld wordt niet getoond aan anderen.</span>
					</section>
					<section class="clearfix form-labeled-field-full">
						{!! Form::label('streetnumber', 'Huisnummer', ['class' => '']) !!}
						{!! Form::text('streetnumber', Auth::User()->streetnumber , ['class' => 'input_bottom_border', 'placeholder' => 'ex. 123']) !!}
						<span class="form-field-notes"> Dit veld wordt niet getoond aan anderen.</span>
					</section>
					<section class="clearfix form-labeled-field-full">
						{!! Form::label('zipcode', 'Postcode', ['class' => '']) !!}
						{!! Form::text('zipcode', Auth::User()->zipcode , ['class' => 'input_bottom_border', 'placeholder' => 'ex. 2000']) !!}
					</section>
					<section class="clearfix form-labeled-field-full">
						{!! Form::label('city', 'Gemeente', ['class' => '']) !!}
						{!! Form::text('city', Auth::User()->city , ['class' => 'input_bottom_border', 'placeholder' => 'ex. Antwerp']) !!}
					</section>
					<section class="clearfix form-labeled-textarea-full">
						{!! Form::label('about', 'Bio', ['class' => '']) !!}
						{!! Form::textarea('about', Auth::User()->about , ['class' => 'input_bottom_border', 'placeholder' => "Schrijf hier iets kort over jezelf, je liefde voor fietsen, ...", 'rows' => '3']) !!}
					</section>
					<section class="form-button-wrapper align-button-right">
						{!! Form::submit('Bijwerken', ['class' => 'btn btn-default'])!!}
					</section>
				{!! Form::close() !!}
			</section>
		</section>
	</section>
@stop

@section('scripts')
	<script>
		document.getElementById("profilepicture").onchange = function() {
			document.getElementById("profilepicchanger").submit();
		};
	</script>
	<script>
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('.user_picture-hover').each(function(){
				$(this).show();
			});
		}
	</script>
	@yield('completerscripts')
@stop