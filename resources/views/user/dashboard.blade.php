@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	@include("user.partials.dashboard_menu", ['active' => 'overview'])
	<section class="user-page clearfix">

		@include("general.partials.alerts")

		@include("user.partials.dashboard_side_menu")
		<section class="dashboard_content">
			<div class="header-image clearfix" style="background:url({!! asset('/images/brand.jpg') !!});">
				<h1>Welkom, <em>{{Auth::user()->firstname}}</em></h1>
				<p>De Shift community wacht op jouw!</p>
				<p>Momenteel zijn wij op zoek naar personen die interesse hebben om Shift in de toekomst te gaan gebruiken. Je kan een account aanmaken en al fietsen toevoegen, het effectief huren of verhuren van fietsen is nog niet mogelijk.</p>
			</div>
			
			@include('user.partials.dashboard_notification_verify_email', ['user' => Auth::user()])
			
			<section class="dashboard_stats">
				<section class="dashboard_title_area">Statistieken</section>
				<section class="dashboard_content_item">
					<section class="flex">
						<section class="dashboard_stats_item bikes">
							<i class="fa fa-bicycle" aria-hidden="true"></i>
							<p>Fietsen geplaatst</p>
							<p class="dashboard_stats_counter">{{ $bikescount }}</p>
							<!--<section class="bike_figures">
								 <p>Met in totaal</p>
								<span>{{ $bikecomments }} <i class="fa fa-comment"></i></span>
								<span>{{ $bikelikes }} <i class="fa fa-heart"></i></span> 
							</section>-->
						</section>
						<section class="dashboard_stats_item">
							<i class="fa fa-bicycle" aria-hidden="true"></i>
							<p>Fietsen gehuurd</p>
							<p class="dashboard_stats_counter">{{ $purchasecount }}</p>
						</section>
						<section class="dashboard_stats_item">
							<i class="fa fa-bicycle" aria-hidden="true"></i>
							<p>Fietsen verhuurd</p>
							<p class="dashboard_stats_counter">{{ $salescount }}</p>
						</section>
					</section>
					
					<section class="flex">
						<section class="dashboard_stats_item">
							<i class="fa fa-comments-o" aria-hidden="true"></i>
							<p>Reacties geplaatst</p>
							<p class="dashboard_stats_counter">{{ $commentedcount }}</p>
						</section>
						<section class="dashboard_stats_item">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<p>Fietsen geliked</p>
							<p class="dashboard_stats_counter">{{ $likedcount }}</p>
						</section>
						<section class="dashboard_stats_item">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<p>Berichten gestuurd</p>
							<p class="dashboard_stats_counter">{{ $messagedcount }}</p>
						</section>
					</section>
				</section>
			</section>
		</section>
		
	</section>
@stop

@section('scripts')
	<script src="/js/libs/progressbar.min.js"></script>
	<script>
		document.getElementById("profilepicture").onchange = function() {
			document.getElementById("profilepicchanger").submit();
		};
	</script>
	<script>
		$(document).ready(function(){
			$('.dashboard_reminder').each(function(){
				$(this).slideDown(400);
			});
		});
	</script>
	@yield('completerscripts')
@stop
