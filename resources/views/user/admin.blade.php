@extends('layouts.default')
@section('navigation')

@stop

@section('content')
	<section>
		@include("user.partials.dashboard_menu", ['active' => 'admin'])
		Hey {{Auth::user()->firstname}}, you're an admin and you can track our growth here:
	</section>
@stop

@section('scripts')
	@yield('completerscripts')
@stop
