<section class="dashboard_notification_item">
	<a class="hide_notification_button read_notification_button" href="#"><i class="fa fa-times"></i></a>
	{!! Form::hidden('notification_id', $notification->id ,[])!!}
	@if($notification->order_id != null)
		@if($notification->order->status == 'sent')
			<p>Nieuwe reservatie voor <b>{{ $notification->order->bike->title }}</b></p>
		@elseif($notification->order->status == 'accepted')
			<p>Uw reservatie voor <b>{{ $notification->order->bike->title }}</b> is goedgekeurd!</p>
		@elseif($notification->order->status == 'declined')
			<p>Uw reservatie voor <b>{{ $notification->order->bike->title }}</b> is geweigerd.</p>
		@elseif($notification->order->status == 'paid')
			<p>Reservatie voor <b>{{ $notification->order->bike->title }}</b> is betaald!</p>
			<p>Noteer de datum in uw agenda zodat u deze afspraak niet mist!</p>
		@endif
		<a class="btn btn-small btn-default" href="{{url('/order/' . $notification->order->id)}}">Bekijk</a>
	@elseif($notification->comment_id != null)
		<p>Nieuwe reactue op <b>{{ $notification->comment->bike->title }}</b> door <a href="{{url('/users/show/' . $notification->comment->user->id)}}">{{ $notification->comment->user->firstname }} {{ $notification->comment->user->lastname }}</a></p>
		<a class="btn btn-small btn-default" href="{{url('/bikes/' . $notification->comment->bike->id . '#comment-section')}}">Bekijk</a>
	@elseif($notification->message_id != null)
		<p>Nieuw bericht van {{ $notification->message->user->firstname }} {{ $notification->message->user->lastname }}</p>
		<a class="btn btn-small btn-default" href="{{url('/users/' . $notification->message->user->id . '/message')}}">Bekijk</a>
	@elseif($notification->like_id != null)
	<p><a href="{{url('/users/show/' . $notification->like->user->id)}}">{{ $notification->like->user->firstname }} {{ $notification->like->user->lastname }}</a> has liked your bike!</p>
		<a class="btn btn-small btn-default" href="{{url('/bikes/' . $notification->like->bike->id)}}">Bekijk</a>
	@endif
</section>