<section id="dashboard-menu">
	<div id="user-info" class="clearfix">
		@if(Auth::User()->image)		
		<img src="/{{Auth::user()->image->thumbnail_path}}" alt="">
		@else
		<img src="{!! asset('/images/placeholders/user-default.png') !!}" alt="">
		@endif

		<h2>{{Auth::user()->firstname}}</h2>

		<h3><a href="{{url('/dashboard/me')}}">Profiel bewerken</a></h3>

		{!! Form::open(array('url' => '/users/addpicture', 'method' => 'POST', 'files' => true, 'id' => 'profilepicchanger'))!!}
		{{ csrf_field() }}
		{!! Form::file('profilepicture', ['class' => 'input-not-displayed', 'id' => "profilepicture"]) !!}
		{!! Form::label('profilepicture', 'Kies nieuwe foto', ['class' => 'btn btn-small btn-default']) !!}
		{!! Form::close() !!}

	</div>
	
	<hr>

	<ul class="dashboard-nav dashboard-bikes">
		<li class="clearfix">
			<a class="clearfix" href="{{url('/bikes')}}">
			<span class="icon"><i class="fa fa-bicycle"></i></span>
			<span class="menu-link">Fietsen</span>
			</a>
		</li>
	</ul>

	<hr>

	<ul class="dashboard-nav">
		<li @if($active == 'overview') class="active clearfix" @else class="clearfix" @endif>
			<a class="clearfix" href="{{url('/dashboard')}}">
			<span class="icon"><i class="fa fa-anchor"></i></span>
			<span class="menu-link">Overzicht</span>
			</a>
		</li>
		<li @if($active == 'profile') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{url('/dashboard/me')}}">
			<span class="icon"><i class="fa fa-user"></i></span>
			<span class="menu-link">Profiel</span>
			</a>
		</li>
		<li @if($active == 'settings') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{url('/dashboard/settings')}}">
			<span class="icon"><i class="fa fa-gears"></i></span>
			<span class="menu-link">Instellingen</span>
			</a>
		</li>
		<!--<li @if($active == 'reservations') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{url('/dashboard/reservations')}}">
			<span class="icon"><i class="fa fa-tags"></i></span>
			<span class="menu-link">Reservaties</span>
			</a>
		</li>-->
		<li @if($active == 'notifications') class="active clearfix notification-tab" @else class="clearfix notification-tab" @endif >
			<a class="clearfix" href="{{url('/dashboard/notifications')}}">
			<span class="icon"><i class="fa fa-bell"></i></span>
			<span class="menu-link">Meldingen</span>
			<span class="notification-counter" @if(Auth::User()->newNotifications()->count() == 0) style="display:none;" @endif >{{Auth::User()->newNotifications()->count()}}</span>
			</a>
		</li>
		<!--<li @if($active == 'messages') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{url('/dashboard/messages')}}">
			<span class="icon"><i class="fa fa-comments"></i></span>
			<span class="menu-link">Berichten</span>
			</a>
		</li>-->
		@if(Auth::user()->admin == '1')
		<li @if($active == 'admin') class="active clearfix" @else class="clearfix" @endif >
			<a class="clearfix" href="{{url('/admin')}}">
			<span class="icon"><i class="fa fa-bar-chart"></i></span>
			<span class="menu-link">Admin</span>
			</a>
		</li>
		@endif
	</ul>

</section>