<ul class="dashboard_side_menu clearfix">
	@if(!Request::is('dashboard/settings'))
		@include('user.partials.dashboard_notification_profile_completer', ['user' => Auth::user()])
		<li class="dashboard_side_menu_refer">
			<section class="dashboard_title_area">Invite your friends</section>
			<section class="content">
				{!! Form::open(array('action' => 'UsersController@inviteFriend', 'method' => 'POST'))!!}
  					{{ csrf_field() }}
	  				{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'your.friend@email.com']) !!}
	    			{!! Form::submit('Invite friend', ['class' => 'btn btn-default-inverse'])!!}
	  			{!! Form::close() !!}
			</section>
		</li>
		<li class="dashboard_side_menu_verification">
			<section class="dashboard_title_area">Verification</section>
			<section class="content">
				<section class="verification_level">
					<?php 
						$totalnumberofstars = 0;
						
						if(Auth::user()->email_verification == 1)
						{
							$totalnumberofstars += 1;
						}
						if(Auth::user()->facebook_verification == 1)
						{
							$totalnumberofstars += 1;
						}
						if(Auth::user()->twitter_verification == 1)
						{
							$totalnumberofstars += 1;
						}
						if(Auth::user()->visa_verification == 1)
						{
							$totalnumberofstars += 1;
						}
					?>
					@for ($i = 1; $i <= $totalnumberofstars; $i++)
						<i class="fa fa-star"></i>
					@endfor
					@for ($i = 1; $i <= 5 - $totalnumberofstars; $i++)
						<i class="fa fa-star-o"></i>
					@endfor
					
				</section>
				<section class="verification_types">
					@if($totalnumberofstars == 0)
						No verifications have been added yet
					@else
						@if(Auth::user()->email_verification != 0)
							<section>
								<i class="fa fa-check"></i> Email
							</section>
						@endif
						@if(Auth::user()->facebook_verification != 0)
							<section>
								<i class="fa fa-check"></i> Facebook
							</section>
						@endif
						@if(Auth::user()->twitter_verification != 0)
							<section>
								<i class="fa fa-check"></i> Twitter
							</section>
						@endif
						@if(Auth::user()->visa_verification != 0)
							<section>
								<i class="fa fa-check"></i> Visa
							</section>
						@endif
					@endif
				</section>
				<section class="dashboard_side_menu_links">
					<a href="{{url('/dashboard/settings#dashboard_verification_settings')}}">Add verification</a>
				</section>
			</section>
		</li>

	@endif
	@if(Request::is('dashboard/settings'))
		<li class="dashboard_side_menu_verification_settings">
			<section class="dashboard_title_area">Settings overview</section>
			<section class="content">
				<a href="#dashboard_notification_settings">Meldingen</a>
				<a href="#dashboard_payment_settings">Betaalmethoden</a>
				<a href="#dashboard_privacy_settings">Privacy</a>
				<a href="#dashboard_verification_settings">Verificatie</a>
				<a href="#dashboard_account_settings">Account</a>
			</section>
		</li>
	@endif
</ul>