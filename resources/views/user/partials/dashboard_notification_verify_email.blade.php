@if($user->email_verification != 1)
	<section class="dashboard_reminder">
		<section class="dashboard_title_area">Shift reminder</section>
		<section class="dashboard_content_item">
			<section class="dashboard_reminder_item">
				<p>Je email adress is nog niet geverifieerd</p>
				<p>Gelieve dit zo snel mogelijk te doen!</p>
				<a class="btn btn-small btn-default" href="{{url('/users/resendverifyemail')}}">Stuur verificatie mail</a>
			</section>
		</section>
	</section>
@endif