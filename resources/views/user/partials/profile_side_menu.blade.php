<ul class="profile_side_menu">
	<!-- <li class="profile_side_menu_user_details">
		@if($user->image)
			<span class="user_picture" style="background-image:url(/{{$user->image->thumbnail_path}});">
		@else
			<span class="user_picture" style="background-image:url({!! asset('/images/placeholders/user-default.png') !!});">
		@endif
		</span>
	</li> -->
	<li class="profile_side_menu_verification">
		<section class="dashboard_title_area">Verification</section>
		<section class="content">
			<section class="verification_level">
					<?php 
						$totalnumberofstars = 0;
						
						if($user->email_verification == 1)
						{
							$totalnumberofstars += 1;
						}
						if($user->facebook_verification == 1)
						{
							$totalnumberofstars += 1;
						}
						if($user->twitter_verification == 1)
						{
							$totalnumberofstars += 1;
						}
						if($user->visa_verification == 1)
						{
							$totalnumberofstars += 1;
						}
					?>
					@for ($i = 1; $i <= $totalnumberofstars; $i++)
						<i class="fa fa-star"></i>
					@endfor
					@for ($i = 1; $i <= 5 - $totalnumberofstars; $i++)
						<i class="fa fa-star-o"></i>
					@endfor
				</section>
			<section class="verification_types">
					@if($totalnumberofstars == 0)
						No verifications have been added yet
					@else
						@if($user->email_verification != 0)
							<section>
								<i class="fa fa-check"></i> Email
							</section>
						@endif
						@if($user->facebook_verification != 0)
							<section>
								<i class="fa fa-check"></i> Facebook
							</section>
						@endif
						@if($user->twitter_verification != 0)
							<section>
								<i class="fa fa-check"></i> Twitter
							</section>
						@endif
						@if($user->visa_verification != 0)
							<section>
								<i class="fa fa-check"></i> Visa
							</section>
						@endif
					@endif
				</section>
		</section>
	</li>
	<li class="profile_side_menu_verification">
		<section class="dashboard_title_area">Friends (0)</section>
		<section class="content">
			Deze feature bestaat nog niet.
		</section>
	</li>
</ul>