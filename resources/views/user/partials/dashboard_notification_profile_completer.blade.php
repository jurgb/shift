<?php
	$percentage = 0;
	if($user->street != ""){
		$percentage += 10;
	}
	if($user->streetnumber != ""){
		$percentage += 10;
	}
	if($user->city != ""){
		$percentage += 20;
	}
	if($user->firstname != "" & $user->lastname != ""){
		$percentage += 20;
	}
	if($user->about != ""){
		$percentage += 20;
	}
	if($user->image){
		$percentage += 20;
	}
?>
<li class="dashboard_side_menu_profile_completer">
	<section class="dashboard_title_area">Profile status</section>
	<section class="content">
		<p id="profilecompleteprogressbarContainer"></p>
		@if($percentage < 100)
			<p>Gelieve je profiel te vervolledigen!</p>
		@else
			<p>Bedankt om je profiel te vervolledigen!</p>
		@endif
		<a class="btn btn-small btn-default" href="/dashboard/me">Vervolledig</a>
	</section>
</li>

@section('completerscripts')
	<script>
		$(document).ready(function(){
			var profilecompleteprogress = <?php echo $percentage ?>;
			if(profilecompleteprogress != 100)
			{
				var profilecompleteprogressbarcolor = '#ff6c00';
			} else {
				var profilecompleteprogressbarcolor = 'rgba(152,212,144,1)';
			}
			var profilecompleteprogressBar = new ProgressBar.Circle('#profilecompleteprogressbarContainer', {
					color: profilecompleteprogressbarcolor,
					trailColor: 'rgba(235,235,235,1)',
					duration: 1000,
					easing: 'easeOut',
					strokeWidth: 5
			});
			profilecompleteprogressBar.set(profilecompleteprogress / 100);
			profilecompleteprogressBar.setText(profilecompleteprogress + '% voltooid');
    });
	</script>
@stop