@if($item->status == 'sent' || $item->status == 'accepted')
	<section class="reservation_preview_wrapper reservation_progress flex">
@endif
@if($item->status == 'paid')
	<section class="reservation_preview_wrapper reservation_completed flex">
@endif
@if($item->status == 'declined' || $item->status == 'cancelled')
	<section class="reservation_preview_wrapper reservation_denied flex">
@endif
	<section class="reservation_bike">
		{{ $item->bike->title }}
	</section>
	<section class="reservation_status">
		{{ $item->status }}
	</section>
	<section class="reservation_pickup">
		<p>Op {{ date('j F Y', strtotime($item->pickup)) }}</p>
		<p>Om {{ date('H\h i', strtotime($item->pickup)) }}</p>
	</section>
	<section class="reservation_dropoff">
		<p>Op {{ date('j F Y', strtotime($item->dropoff)) }}</p>
		<p>Om {{ date('H\h i', strtotime($item->dropoff)) }}</p>
	</section>
	<section class="reservation_price">
		&euro; {{ $item->price }}
	</section>
	<section class="reservation_actions">
		<a class="btn btn-small btn-default" href="{{url('/order/' . $item->id)}}">Bekijken</a>
	</section>
</section>