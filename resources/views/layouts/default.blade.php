<!DOCTYPE html>
<html lang="nl">
	<head>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="keywords" content="fiets delen, fietsen, fiets huren, fiets verhuren, Shift, deeleconomie, bikesharing, share your bike, deelplatform">
    <meta name="description" content="Shift is een deelplatform voor fietsen gemaakt door en voor bike-lovers. Word nu gratis lid van een unieke bike-minded community!">
    <meta name="author" content="Shift">
    <meta name="_token" content="{{ csrf_token() }}">

    <!-- FB open graph -->
    <meta property="og:url" content="http://goshift.be"/>
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Shift - Rent a bike from a local."/>
    <meta property="og:image" content="http://goshift.be/images/shift-viral.jpg">
    <meta property="og:description" content="Shift is een deelplatform voor fietsen gemaakt door en voor bike-lovers. Word nu gratis lid van een unieke bike-minded community!"> 

    <!-- TW cards-->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@goshiftbe">
    <meta name="twitter:creator" content="@goshiftbe">
    <meta name="twitter:title" content="Shift - Rent a bike from a local.">
    <meta name="twitter:description" content="Shift is een deelplatform voor fietsen gemaakt door en voor bike-lovers. Word nu gratis lid van een unieke bike-minded community!">
    <meta name="twitter:image" content="http://goshift.be/images/shift-viral.jpg">

   
    <title>Shift | Rent a bike from a local.</title>
   
    <link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/images/icons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/icons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/images/icons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/images/icons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/images/icons/manifest.json">
    <link rel="mask-icon" href="/images/icons/safari-pinned-tab.svg" color="#f58230">
    <link rel="shortcut icon" href="/images/icons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/images/icons/mstile-144x144.png">
    <meta name="msapplication-config" content="/images/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>

  <body>
  	<div class="page-wrap">
    	@include('components.navigation.navigation')
    	@yield('headerimage')
      <main role="main">      
      	@yield('content')
     	</main>
     	<script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
      <script src="{!! asset('js/app.js') !!}"></script>
  		@yield('scripts')
    </div>
    @include('components.footer.footer')
    
    <link href="{!! asset('css/fonts/opensans.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="{!! asset('css/app.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/fonts/opensans.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/font-awesome.min.css') !!}" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="/js/libs/progressbar.min.js"></script>
    
    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
			<script src="//code.tidio.co/imec3rrt2jakazsyc2o2nmu1zusbryli.js"></script>
		<?php endif; ?>
		
		<!-- GOOGLE ANALYTICS -->
    <script type="text/javascript">
      var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
      document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
      try{
      var pageTracker = _gat._getTracker("UA-53879290-4");
      pageTracker._trackPageview();
      } catch(err) {}
    </script>
    
    <script type="text/javascript">
			$.ajaxSetup({
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
			});
		</script>
  </body>
</html>