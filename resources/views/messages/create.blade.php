@extends('layouts.default')
@section('navigation')

@stop


@section('headerimage')

@stop
@section('content')
		@include("user.partials.dashboard_menu", ['active' => 'messages'])

		@include("general.partials.alerts")

	<section class="conversation-wrapper user-page">
		
		<section class="conversation-messages-wrapper messages_content clearfix">
			@include("general.partials.alerts")
			<div class="header-image clearfix" style="background:url({!! asset('/images/brand.jpg') !!});">
				<h1 class="title-header">Zeg eens dag tegen {{$user->firstname}}</h1>
				<p>De Shift community wacht op jou!</p>
				<p>Hier kan je jouw berichten vinden, het is altijd leuk om je ervaringen te delen met andere <em>Shifters</em>!</p>
			</div>

			<ol class="breadcrumb">
			  <li><a href="/dashboard">Dashboard</a></li>
			  <li><a href="/dashboard/messages">Berichten</a></li>
			  <li class="active">{{$user->firstname}}</li>
			</ol>

		@if(isset($conversation))
			@foreach($conversation->messages as $message)
				<section @if($message->user_id == Auth::User()->id) class="conversation-message self list-group" @else class="conversation-message list-group" @endif >
					<span class="list-group-item">
						@if($message->user_id != Auth::User()->id)
							@if($user->image)
								<img src="/{{$user->image->thumbnail_path}}" alt="" class="img-circle">
							@else
								<img src="{!! asset('/images/placeholders/user-default.png') !!}" alt="" class="img-circle">
							@endif
						@endif

						<span class="conversation-message-content">{!! $message->content !!}</span> 
						<span class="conversation-message-timestamp text-right">{{$message->created_at->diffForHumans()}}</span>
					</span>
				</section>
			@endforeach
		@else
			<span class="list-group empty-state">
				<p class="list-group-item">Twijfel niet, stuur {{ $user->firstname }} een berichtje!</p>
			</span>		
		@endif
		</section>
		<section class="conversation-add-message-form">

			@if(Auth::User()->image)
				<img src="/{{Auth::user()->image->thumbnail_path}}" alt="" class="img-circle">
			@else
				<img src="/images/placeholders/default.png" alt="" class="img-circle">
			@endif


			{!! Form::open(['url' => '/users/' . $user->id .'/message']) !!}
				{{ csrf_field() }}
				@if(isset($conversation))
					{!!Form::hidden('conversationid', $conversation->id) !!}
				@endif
				{!! Form::textarea('content', "", ['class' => 'input_bottom_border','rows' => '3','placeholder' => 'Jouw bericht:']) !!}
				<section class="form-button-wrapper">
					{!! Form::submit("Verstuur bericht", ['class' => 'btn btn-small btn-default']) !!}
				</section>	
			{!! Form::close() !!}
		</section>
	</section>
@stop

@section('scripts')

<script>
	document.getElementById("profilepicture").onchange = function() {
		document.getElementById("profilepicchanger").submit();
	};
</script>	

@stop
