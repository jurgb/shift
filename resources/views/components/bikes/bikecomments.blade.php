<section id="comment-section" class="comment_container">
	<h2 class="formheader"><i class="fa fa-comment-o"></i> Reacties</h2>
	@include('errors.formerrors')
	<ul class="comment_block">
		@if($bike->comments->count())
			@foreach($bike->comments as $comment)
				<li class="comment_block_wrapper clearfix">
					<div class="comment_block_user_image_wrapper">
						<a href="{{url('users/show/' . $comment->user->id)}}">
							<span class="comment_block_user_image" @if($comment->user->image) style="background-image: url('/{{$comment->user->image->thumbnail_path}}') " @endif>
							</span>
						</a>
					</div>
					<div class="comment_block_content_wrapper">
						<span class="comment_block_user_details">
							<span class="comment_block_user_name">
								<a href="{{url('users/show/' . $comment->user->id)}}">
									{{$comment->user->firstname}}
									{{$comment->user->lastname}}
								</a>
							</span>
							<span class="comment_block_user_comment_details">
								{{$comment->created_at->diffForHumans()}}
							</span>
						</span>
						<div class="comment_block_content">
						{{ $comment->comment }}
						</div>
					</div>
					@if(Auth::User())
						@if($comment->user->id == Auth::User()->id)
							<div class="comment_block_user_comment_actions">
								{!!Form::open(['url' => '/bikes/' . $bike->id . '/comment/' . $comment->id , 'method' => 'post','onsubmit' => 'return ConfirmDelete()']) !!}
									{{ csrf_field() }}
									<button type="submit" id="delete_comment_button" class="btn btn--small"><i class="fa fa-trash-o fa-lg"></i></button>
								{!! Form::close() !!}
							</div>
						@endif
					@endif
				</li>
			@endforeach
		@else
		<li class="comment_block_into">Reageer als eerste!</li>
		@endif
		<li class="comment_loading clearfix" style=""><p class="comment_loading_text"></p><i class="fa fa-spinner fa-spin fa-2x"></i></li>
	</ul>
	<div class="comment_block_form_wrapper">
		@if(Auth::User())
			{!! Form::open(['url' => '/bikes/'. $bike->id . '/comment']) !!}
				{{ csrf_field() }}
				{!! Form::hidden('linkadres', '/bikes/' . $bike->id . '/comment') !!}
				{!! Form::label('comment', 'Reageer als ' . Auth::User()->firstname . ' ' . Auth::User()->lastname, ['class' => 'form-label-full']) !!}
				{!! Form::textarea('comment', "", ['class' => 'input_bottom_border','rows' => '6','placeholder' => 'Voeg een reactie toe']) !!}
				<section class="form-button-wrapper">
					{!! Form::submit("Reactie plaatsen", ['class' => 'btn btn-small btn-default', 'id' => 'add-comment-button']) !!}
				</section>	
			{!! Form::close() !!}
		@else
			<p class="comment-loginmessage"><a href="{{url('/login')}}">Login</a> om te kunnen reageren</p>
		@endif
	</div>
</section>

@section('filter.scripts')
	<script>
		$(document).ready(function(){
			function escapeHtml(text) {
				var map = {
					'&': '&amp;',
					'<': '&lt;',
					'>': '&gt;',
					'"': '&quot;',
					"'": '&#039;'
				};

				return text.replace(/[&<>"']/g, function(m) { return map[m]; });
			}
			
			$('#add-comment-button').click(function(e){
				$('.comment_loading').find('.comment_loading_text').text('Reactie wordt geplaatst, even geduld...');
				$('.comment_loading').show();
				
				correctUrl = $('.comment_block_form_wrapper').find('input[name=linkadres]').val();
				correctData ={
							'comment': $('.comment_block_form_wrapper').find('textarea[name=comment]').val(),'_token': $('meta[name="_token"]').attr('content')
				}
				if(/^[a-zA-Z0-9]+$/.test($('.comment_block_form_wrapper').find('textarea[name=comment]').val())){
					var response = $.ajax({
						url: correctUrl,
						data: correctData,
						type: "post",	
						dataType: "json",
						success: function(data){
							if (data.status == "succes"){

								var crsf_token = $('meta[name="_token"]').attr('content');
								var newItem = '<li class="comment_block_wrapper" style="display:none"><div class="comment_block_user_image_wrapper"><a href="/users/show/' + data.userid + '"><span class="comment_block_user_image" style=' + data.background + '></span></a></div><div class="comment_block_content_wrapper"><span class="comment_block_user_details"><span class="comment_block_user_name"><a href="/users/show/' + data.userid + '">' + data.username + '</a></span><span class="comment_block_user_comment_details">Just now</span></span><div class="comment_block_content">' + escapeHtml(data.comment.comment) + '</div></div><div class="comment_block_user_comment_actions"><form method="POST" action="' + data.destroyurl + '" accept-charset="UTF-8" onsubmit="return ConfirmDelete()"><input name="_token" type="hidden" value="' + crsf_token + '"><button type="submit" id="delete_comment_button" class="btn btn--small"><i class="fa fa-trash-o fa-lg"></i></button></form></div></li>';

								$('.comment_loading').before(newItem);
								$('.comment_loading').fadeOut(function(){	
									$('.comment_block_wrapper').slideDown(400);
									$('.comment_block_form_wrapper').find('textarea[name=comment]').val("");
								});
							}
						},
						error: function(errres){
							$('.comment_loading').hide();
							alert("something went wrong, please try again.");
						}
					});
				} else {
					$('.comment_loading').hide();
					$('.comment_block_form_wrapper').find('textarea[name=comment]').val("");
					if ($('#commentemptyerrorblock').val() != "")
					{
						var newerroritem = "<div id='commentemptyerrorblock' style='display:none;' class='alert alert-danger' role='alert'><ul><li>Gelieve een reactie toe te voegen!</li></ul></div>";
						$('#comment-section').find('.formheader').after(newerroritem);
						$('#commentemptyerrorblock').slideDown(300);
					}
				};
				
				e.preventDefault();
			});
			
			$(".comment_block").on("click", "#delete_comment_button", function(e){
				buttonclicked = $(this);
				headsection = $(this).parent();
				confirmed = ConfirmDelete();
				if(confirmed == true){
					$('.comment_loading').find('.comment_loading_text').text('Reactie wordt verwijderd, even geduld...');
					$('.comment_loading').show();
					
					correctUrl = headsection.attr('action');
					correctData ={
						'_token': $('meta[name="_token"]').attr('content')
					}
					var response = $.ajax({
						url: correctUrl,
						data: correctData,
						type: "post",	
						dataType: "json",
						success: function(data){
							if (data.status == "succes"){
								$('.comment_loading').fadeOut(function(){
									headsection.closest(".comment_block_wrapper").slideUp(400,function(){
										headsection.closest(".comment_block_wrapper").remove();
									});
								});
							}
						},
						error: function(errres){
							$('.comment_loading').hide();
							alert("Er ging its mis, herlaad de pagina en probeer opnieuw! Onze excuses voor dit ongemak.");
						}
					});
				}
				e.preventDefault();
			});
		});
	</script>
@stop