<section class="bikepreview_wrapper">
	<section class="bikepreview_image" @if($bike->image) style="background-image:url(/{{$bike->image->thumbnail_path}})" @endif>
	<span class="photo-available">{{ $bike->photos()->count() }} <i class="fa fa-camera"></i></span>
		<a href="{{url('/bikes/' . $bike->id)}}">
			<section class="bikepreview_hover">
				<p><i class="fa fa-bicycle"></i></p>
				<p class="bike-type">{{ $bike->type }}</p>
				<p class="bike-location"><i class="fa fa-map-marker"></i> {{ $bike->city }}</p>
				<p class="bike-accessories">
					<i class="fa fa-lock"></i>
					<i class="fa fa-gears"></i>
					<i class="fa fa-suitcase"></i>
					<i class="fa fa-child"></i>
				</p>
			</section>
		</a>
	</section>
	<section class="bikepreview_content">
		<section class="bikepreview_content_user">
			<a href="{{url('users/show/' . $bike->user->id)}}">
				@if($bike->user->image)
					<p class="bikepreview_content_user_picture" style="background-image:url(/{{$bike->user->image->thumbnail_path}});"></p>
				@else
					<p class="bikepreview_content_user_picture" style="background-image:url({!! asset('/images/placeholders/user-default.png') !!});"></p>
				@endif
			</a>
		</section>
		<section class="bikepreview_content_item">
			<a class="bikepreview_content_title" href="{{url('/bikes/' . $bike->id)}}">{{ $bike->title }}</a>
			<p class="bikepreview_content_description">{{ str_limit($bike->description, 80) }}</p>
		</section>
	</section>
	<section class="bikepreview_footer">
		<!--<section class="bikepreview_footer_button">
			<a class="btn btn-small btn-default-inverse" href="{{url('/bikes/' . $bike->id)}}">&euro; {{str_replace(".",",",$bike->price)}}</a>
		</section>
		<section class="bikepreview_footer_actions">
			<a class="btn btn-social-action-small" href="{{url('/bikes/' . $bike->id . '#comment-section')}}"> {{$bike->comments->count()}} <i class="fa fa-comment"></i></a>
			@if(Auth::user())
				@if(Auth::user()->id != $bike->user_id)
					<a class="btn btn-social-action-small btn_like_bike" href="{{url('/bikes/' . $bike->id . '/like')}}"> 
						{{$bike->likes->count()}} 
						@if($bike->likes()->where('user_id', Auth::user()->id)->first())	
							<i class="fa fa-heart"></i>
						@else
							<i class="fa fa-heart-o"></i>
						@endif
					</a>
				@else
					<a class="btn btn-social-action-small" href="#"> {{$bike->likes->count()}} <i class="fa fa-heart"></i></a>
				@endif
			@else
				<a class="btn btn-social-action-small" href="#"> {{$bike->likes->count()}} <i class="fa fa-heart"></i></a>
			@endif
<!--			<a class="btn btn-social-action-small" href="#">2 <i class="fa fa-share"></i></a>
		</section>-->
	</section>
</section>