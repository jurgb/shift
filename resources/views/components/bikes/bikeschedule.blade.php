<section class="bikeschedule">
	<p class="bikeschedule_title"><i class="fa fa-clock-o"></i> Beschikbaar op</p>
	<section class="bikeschedule_day">
		@if( $bike->schedule->first()->monday == "true")
			<i class="fa fa-check-circle"></i>
		@else
			<i class="fa fa-times-circle"></i>
		@endif
		<p>Maandag</p>
	</section>
	<section class="bikeschedule_day">
		@if( $bike->schedule->first()->tuesday == "true")
			<i class="fa fa-check-circle"></i>
		@else
			<i class="fa fa-times-circle"></i>
		@endif
		<p>Dinsdag</p>
	</section>
	<section class="bikeschedule_day">
		@if( $bike->schedule->first()->wednesday == "true")
			<i class="fa fa-check-circle"></i>
		@else
			<i class="fa fa-times-circle"></i>
		@endif
		<p>Woensdag</p>
	</section>
	<section class="bikeschedule_day">
		@if( $bike->schedule->first()->thursday == "true")
			<i class="fa fa-check-circle"></i>
		@else
			<i class="fa fa-times-circle"></i>
		@endif
		<p>Donderdag</p>
	</section>
	<section class="bikeschedule_day">
		@if( $bike->schedule->first()->friday == "true")
			<i class="fa fa-check-circle"></i>
		@else
			<i class="fa fa-times-circle"></i>
		@endif
		<p>Vrijdag</p>
	</section>
	<section class="bikeschedule_day">
		@if( $bike->schedule->first()->saturday == "true")
			<i class="fa fa-check-circle"></i>
		@else
			<i class="fa fa-times-circle"></i>
		@endif
		<p>Zaterdag</p>
	</section>
	<section class="bikeschedule_day">
		@if( $bike->schedule->first()->sunday == "true")
			<i class="fa fa-check-circle"></i>
		@else
			<i class="fa fa-times-circle"></i>
		@endif
		<p>Zondag</p>
	</section>
</section>