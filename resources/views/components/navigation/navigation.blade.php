<nav id="mainnav" class="navbar">
		<section class="top-menu clearfix">
			<div class="clearfix">

			<div class="pull-left">
				<h3 class="site-slogan"><strong>Shift</strong>, the bike-minded community.</h3>
			</div>
			<ul class="secundary-menu nav navbar-nav pull-right">
				@if(Auth::user())
					<li class="dropdown user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span>Hi, {{Auth::user()->firstname }}</span><span class="caret"></span>
							@if(Auth::User()->image)
								<span class="nav-user-picture" style="background-image:url(/{{Auth::user()->image->thumbnail_path}});">
							@else
								<span class="nav-user-picture" style="background-image:url({!! asset('/images/placeholders/user-default.png') !!});">
							@endif
							<span class="notification-counter nav-counter" @if(Auth::User()->newNotifications()->count() == 0) style="display:none;" @endif >{{Auth::User()->newNotifications()->count()}}</span>
						</a>

						<ul class="dropdown-menu">
							<li><a href="{{url('/dashboard')}}">Mijn profiel</a></li>
			                <li><a href="{{url('/dashboard/notifications')}}">Meldingen ({{Auth::User()->newNotifications()->count()}})</a></li>
			                <li class="divider"></li>
			                <li>
								<a href="{{url('/logout')}}">Afmelden</a>
							</li>
			            </ul>

					</li>
				@else
					<li><a href="{{url('/login')}}">Inloggen</a></li>
					<li><a href="{{url('/register')}}">Registreren</a></li>
				@endif				
			</ul>
			</div>			
		</section>
		
		<div class="clearfix">

			<section class="pull-left">
				<a class="logo-link" href="{{url('/')}}"><img src="/images/logo.svg" alt="Logo shift"></a>
				<span class="brand"><a href="{{url('/')}}">Shift</a></span>
			</section>
			
			<ul class="main-menu pull-right">
				<li><a href="/">Shift</a></li>
				<li><a href="{{url('/bikes')}}">Fietsen</a></li>
				<li><a href="{{url('/about')}}">Wie zijn wij</a></li>
				<li><a href="/#shiftwithus"><span class="sparkle">#</span>shiftwithus</a></li>
				<li><a class="btn-sparkle" href="{{url('/bikes/create')}}">Fiets toevoegen</a></li>
			</ul>
			
			<div class="pull-right" id="hamburger">
				<a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
			</div>

		</div>

		<div id="main-nav-mobile">
			<ul class="mobile-menu">
				<li><a href="/">Shift</a></li>
				<li><a href="{{url('/bikes')}}">Fietsen</a></li>
				<li><a href="">Wie zijn wij</a></li>
				<li><a href="/#shiftwithus"><span class="sparkle">#</span>shiftwithus</a></li>
				<li><a class="" href="{{url('/bikes/create')}}">Fiets verhuren</a></li>
			</ul>
		</div>
</nav>