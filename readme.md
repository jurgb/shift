#Installation guide for Jens, Yannick and Jurgen

Ok, de .env file zit er mee in maar het kan zijn da je een nieuwe app key gaat moeten genereren voor uzelf persoonlijk.
Run volgend cmmand:

php artisan key:generate

Als alles goed gaat word deze key automatisch in je .env file gezet. Als dat niet het geval is moet je deze maar eve copy pasten.

##database setup

Ik heb ervoor gezorgt dat we lokaal niet moeten inloggen door gebruik te maken van sqlite, iets minder toeganklijk dan mysql maar veel efficiënter voor local dev.
Om dit te doen werken moet je via commandline navigeren naar /shift/storage en daar 'touch database.sqlite' uitvoeren. om te testen of dat gelukt is  run je het commando 'php artisan migrate' (voor sommigen is dat gewoon artisan migrate) Als dat zonder problemen is gelukt dan is normaal de basic setup klaar.

Voor extra info over [sqlite](https://www.sqlite.org/docs.html) 
Om sqlite database te raadplegen type je in de /shift folder 'sqlite storage/database.sqlite' en dan kan je selects maken zoals bij mysql (vb. select *from users;) De ; op het einde is vrij belangrijk anders kan hij de opdracht niet uitvoeren.


# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
