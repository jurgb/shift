<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateBikeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
					'title' => 'required|string',
					'price' => 'required|regex:/^[0-9]+(\,[0-9]{1,2})?$/|min:1',
					'description' => 'required|string',
					'type' => 'required|string',
					'street' => 'required|string',
					'streetnumber' => 'required|string',
					'zipcode' => 'required|string',
					'city' => 'required|string',
        ];
    }
}
