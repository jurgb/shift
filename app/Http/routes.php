<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Authentication & registration Routes
|--------------------------------------------------------------------------
*/
Route::auth();

/*
|--------------------------------------------------------------------------
| General Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'GeneralController@showLanding');
Route::get('/about', 'GeneralController@showAbout');

/*
|--------------------------------------------------------------------------
| Users
|--------------------------------------------------------------------------
*/
Route::group(array('middleware' => 'auth'), function() {

  Route::get('/users/show/{userid}', 'UsersController@showUser');
  Route::get('/dashboard', 'UsersController@showDashboard');
  Route::get('/dashboard/me', 'UsersController@showProfile');
  Route::get('/dashboard/settings', 'UsersController@showSettings');
  Route::get('/dashboard/notifications', 'UsersController@showNotifications');
	Route::get('/dashboard/messages', 'UsersController@showMessages');
	Route::get('/dashboard/reservations', 'UsersController@showReservations');
	Route::get('/users/verifyemail/{emailkey}', 'UsersController@addEmailVerification');
	Route::get('/users/resendverifyemail', 'UsersController@sendEmailVerification');
	
	Route::post('/users/addpicture', 'UsersController@addPhoto');
	Route::post('/users/update', 'UsersController@update');
	Route::post('/users/addfbverification', 'UsersController@addFBVerification');
	Route::post('/dashboard', 'UsersController@inviteFriend');
	Route::post('/notification/read', 'UsersController@readNotification');
	
});

/*
|--------------------------------------------------------------------------
| Admin
|--------------------------------------------------------------------------
*/
Route::group(array('middleware' => 'App\Http\Middleware\RoleMiddleware'), function() {
	
	Route::get('/admin', 'AdminController@showDashboard');
	
});

/*
|--------------------------------------------------------------------------
|	Bikes
|--------------------------------------------------------------------------
*/

Route::resource('bikes', 'BikeController');

/* Bike routes that require authentication */
Route::group(array('middleware' => 'auth'), function() {
	Route::resource('bikes', 'BikeController', ['except' => [
    'index','show'
	]]);
	
	Route::get('/bikes/{bikeid}/changecover', 'BikeController@showCoverphotos');
	Route::get('/bikes/{bikeid}/changecover/{photoid}', 'BikeController@updateCoverphotos');
	Route::get('/bikes/{bikeid}/addphotos', 'BikeController@showaddphotos');
	
	Route::post('/bikes/{bikeid}/addphotos', 'BikeController@addphotostobike');
	Route::post('bikes/{bikeid}/photos/{photoid}', 'BikeController@removePhoto');
	
/*
|	Bike schedule routes
*/
	Route::get('/bikes/{bikeid}/schedule/create', 'ScheduleController@create');
	Route::get('/bikes/{bikeid}/schedule/edit', 'ScheduleController@edit');
	
	Route::post('/bikes/{bikeid}/schedule/create', 'ScheduleController@store');
	Route::post('/bikes/{bikeid}/schedule/edit', 'ScheduleController@update');
	
	
	Route::post('/bikes/{bikeid}/like', 'BikeController@like');

});

/*
|--------------------------------------------------------------------------
|	Orders
|--------------------------------------------------------------------------
*/

	Route::get('/bikes/{bikeid}/order', 'OrderController@create');
	Route::get('/order/{orderid}', 'OrderController@show');
	Route::post('/order/{orderid}', 'OrderController@pay');
	Route::get('/order/{orderid}/accept', 'OrderController@acceptOrder');
	Route::get('/order/{orderid}/decline', 'OrderController@declineOrder');
	Route::post('/bikes/{bikeid}/order', 'OrderController@store');
	Route::delete('/order/{orderid}', 'OrderController@destroy');

/*
|--------------------------------------------------------------------------
| Comments
|--------------------------------------------------------------------------
*/

	Route::post('/bikes/{bikeid}/comment', 'CommentController@store');
	Route::post('/bikes/{bikeid}/comment/{commentid}', 'CommentController@destroy');

/*
|--------------------------------------------------------------------------
| Messages
|--------------------------------------------------------------------------
*/
	Route::group(array('middleware' => 'auth'), function() {
		Route::get('/users/{userid}/message', 'MessagesController@create');
		
		Route::post('/users/{userid}/message', 'MessagesController@store');
	});