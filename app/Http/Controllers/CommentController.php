<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Bike;
use App\Models\Comment;
use App\Models\Notification;
use Auth;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
					'comment' => 'required'
			]);
			
			$bike = Bike::findOrfail($request->bikeid);
			$activeuser = Auth::User();
			$comment = $bike->comments()->create($request->all());
			$comment->user_id = $activeuser->id;
			$comment->update();
			
			$username = $activeuser->firstname . ' ' . $activeuser->lastname;
			$image = "";
			if($activeuser->image){
				$image = "background-image:url('/" . $activeuser->image->thumbnail_path . "');";
			} else {
				$image = "background-image:url('/images/placeholders/user-default.png');";
			}
			$destroyurl = '/bikes/' . $bike->id . '/comment/' . $comment->id;
			
			$allcommentsonitem = $comment->bike->comments;
			
			$sendnotificationto = $this->unique_multidim_array($allcommentsonitem, 'user_id');
			if($comment->user_id != $bike->user_id)
			{
				$notification = new Notification;
				$notification->user_id = $comment->bike->user->id;
				$notification->comment_id = $comment->id;
				$notification->save();
			}
			foreach($sendnotificationto as $addnotification){
				if($addnotification->user_id != $comment->user_id & $addnotification->user_id != $comment->bike->user->id){
					$notification = new Notification;
					$notification->user_id = $addnotification->user_id;
					$notification->comment_id = $comment->id;
					$notification->save();
				}
			}
			
			if($request->ajax()){
      	return '{"status":"succes","userid":"' . $activeuser->id . '", "username":"' . $username . '", "background":"' . $image . '", "comment":'. $comment .', "destroyurl": "' . $destroyurl .'"}';
      } else {
				return redirect('/bikes/' . $bike->id . '#comment-section');
			}
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $comment = Comment::findOrfail($request->commentid);
			
			foreach ($comment->notifications as $notification) {
				$notification->delete();
			}
			
			$comment->delete();
			
			if($request->ajax()){
      	return '{"status":"succes"}';
      } else {
				return back();
			}
    }
	
		public function unique_multidim_array($array, $key) 
		{ 
			$temp_array = array(); 
			$i = 0; 
			$key_array = array(); 

			foreach($array as $val) { 
					if (!in_array($val[$key], $key_array)) { 
							$key_array[$i] = $val[$key]; 
							$temp_array[$i] = $val; 
					} 
					$i++; 
			} 
			return $temp_array; 
		}
}
