<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class GeneralController extends Controller
{
    public function showLanding()
    {
      return view('general.landing');
    }

    public function showAbout()
    {
      return view('general.about');
    }
}
