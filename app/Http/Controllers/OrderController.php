<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Bike;
use App\Models\Order;
use App\Models\Notification;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Flash;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($bikeid)
    {
			$bike = Bike::findOrfail($bikeid);
      return view('orders.create', compact('bike'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			
			$this->validate($request, [
					'pickup_date' => 'required|date|after:now',
					'pickup_start_time_hour' => 'required',
					'pickup_start_time_min' => 'required',
					'dropoff_date' => 'required|date|after:now',
					'dropoff_start_time_hour' => 'required',
					'dropoff_start_time_min' => 'required'
			]);
			
			$bike = Bike::findOrfail($request->bikeid);
			
			$order = new Order;
			$order->user_id = Auth::User()->id;
			$order->seller_id = $bike->user_id;
			$order->bike_id = $bike->id;
			
			$pickupdate = new Carbon($request->pickup_date);
			$pickupdate->hour = $request->pickup_start_time_hour;
			$pickupdate->minute = $request->pickup_start_time_min;
			
			$order->pickup = $pickupdate;
			
			$dropoffdate = new Carbon($request->dropoff_date);
			$dropoffdate->hour = $request->dropoff_start_time_hour;
			$dropoffdate->minute = $request->dropoff_start_time_min;
			
			$order->dropoff = $dropoffdate;
			
			if($pickupdate->diffInMinutes($dropoffdate, false) < 0)
			{
				return back()->withErrors('Drop off datum moet na de pick up datum komen!');
			}
			
			$hours = ceil($pickupdate->diffInMinutes($dropoffdate) / 60);
			
			$order->price = $hours * $bike->price;
			$order->status = "sent";
			$order->save();
			
			$notification = new Notification;
			$notification->user_id = $order->seller_id;
			$notification->order_id = $order->id;
			$notification->save();
			
    	return redirect('/order/' . $order->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($orderid)
    {
			$order = Order::findOrfail($orderid);
			if($order->user_id == Auth::User()->id | $order->seller_id == Auth::User()->id) {
				
				$notification = Notification::where('user_id','=',Auth::user()->id)->where('order_id','=', $orderid)->first();
				if (count($notification)) 
				{ 
					$notification->seen = 1;
					$notification->update();
				}
				
      	return view('orders.show',compact('order'));
			} else {
				return back();
			}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			$order = Order::findOrfail($id);
      foreach ($order->notifications as $notification) {
				$notification->delete();
			}
			$order->status = 'cancelled';
			$order->update();
			
			return redirect('/order/' . $order->id);
    }
		
		public function acceptOrder($orderid)
    {
      $order = Order::findOrfail($orderid);
			if($order->seller_id == Auth::User()->id) {
      	$order->status = "accepted";
      	$order->update();
			}
			
			$notification = new Notification;
			$notification->user_id = $order->user_id;
			$notification->order_id = $order->id;
			$notification->save();
			
			return back();
			
    }
		public function declineOrder($orderid)
    {
      $order = Order::findOrfail($orderid);
			if($order->seller_id == Auth::User()->id) {
      	$order->status = "declined";
				$order->update();
			}
			
			$notification = new Notification;
			$notification->user_id = $order->user_id;
			$notification->order_id = $order->id;
			$notification->save();
			
			return back();
    }
    public function pay(Request $request)
    {
        $token = $request['stripeToken'];
        $user_id = Auth::user()->id;
        $total_price = $request['amount'];

        $customer = User::find($user_id);
        \Stripe\Stripe::setApiKey('sk_test_wVG4Ia7gItOHshKAQwhQDe9V');
				
		$order = Order::findOrfail($request->orderid);
		
		$totalcharge = $order->price * 100;

//        if($customer->charge($totalcharge,
//            [
//            'source' => $token,
//            'receipt_email' => $customer->email,
//            'currency' => 'eur',
//            "description" => "Pay that order, dude!",
//            ]))
//        {
//					$order->status = 'paid';
//          return redirect('/order/'. $order->id);
//        }else{
//         	$order->status = 'paid';
//          return redirect('/order/'. $order->id);
//        }

        // Pay with Stripe - the other way
        try{

            $charge = \Stripe\Charge::create(array(
            "amount" => $totalcharge,
            "currency" => "eur",
            "source" => $token,
            "description" => "Pay that order, dude!"
            ));

           	$order->status = 'paid';
            $order->update();

            $notification = new Notification;
            $notification->user_id = $order->seller_id;
            $notification->order_id = $order->id;
            $notification->save();


          	return redirect('/order/'. $order->id);

        }catch (\Stripe\Error\Card $e){

			$order->status = 'declined';
          	return redirect('/order/'. $order->id);
        
        }
    }
}
