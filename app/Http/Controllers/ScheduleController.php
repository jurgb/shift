<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Bike;
use App\Models\Bikeschedule;

class ScheduleController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($bikeid)
    {
			$bike = Bike::findOrfail($bikeid);
			$schedule = Bikeschedule::where('bike_id','=',$bikeid)->first();
      return view('bikes.schedule.add',compact('bike','schedule'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$bike = Bike::findOrfail($request->bikeid);
			$schedule = Bikeschedule::where('bike_id','=',$request->bikeid)->first();
    	$schedule->update($request->all());
			$images = $request->bikepictures;
      return redirect('/bikes/'. $bike->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($bikeid)
    {
			$bike = Bike::findOrfail($bikeid);
			$schedule = Bikeschedule::where('bike_id','=',$bikeid)->first();
			return view('bikes.schedule.edit', compact('bike', 'schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $bikeid)
    {
    	$bike = Bike::findOrfail($bikeid);
			$schedule = Bikeschedule::where('bike_id','=',$bikeid)->first();
    	$schedule->update($request->all());
			if(!$request->monday){
				$schedule->monday = "0";
			}
			if(!$request->tuesday){
				$schedule->tuesday = "0";
			}
			if(!$request->wednesday){
				$schedule->wednesday = "0";
			}
			if(!$request->thursday){
				$schedule->thursday = "0";
			}
			if(!$request->friday){
				$schedule->friday = "0";
			}
			if(!$request->saturday){
				$schedule->saturday = "0";
			}
			if(!$request->sunday){
				$schedule->sunday = "0";
			}
			$schedule->update();
			return redirect('/bikes/'. $bike->id);
    }
}
