<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Userphoto;
use App\Models\Message;
use App\Models\Conversation;
use App\Models\Notification;
use App\Models\Order;
use Auth;
use Mail;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UsersController extends Controller
{
    public function showDashboard()
    {
			$activeuser = Auth::User();
			
			$allbikes = $activeuser->bikes;
			$bikescount = $allbikes->count();
			$bikelikes = 0;
			$bikecomments = 0;
			
			foreach($allbikes as $bike)
			{
				$bikelikes += $bike->likes->count();
				$bikecomments += $bike->comments->count();
			}
			
			$purchasecount = $activeuser->orders->where('status','paid')->count();
			$salescount = Order::where('seller_id','=',$activeuser->id)->where('status','=','paid')->count();
			
			$commentedcount = $activeuser->comments->count();
			$likedcount = $activeuser->likes->count();
			
			$messagedcount = $activeuser->messages->count();
			
			
      return view('user.dashboard', compact('bikescount', 'bikelikes','bikecomments','purchasecount','salescount','commentedcount','likedcount','messagedcount'));
    }
		
		public function showProfile()
    {
      return view('user.profile');
    }
	
		public function showUser($userid)
    {
			$user = User::findOrfail($userid);
      return view('user.show', compact('user'));
    }
		
		public function showSettings()
    {
      return view('user.settings');
    }
		
		public function showNotifications()
    {
      return view('user.notifications');
    }
		
		public function showMessages()
    {
			if(Auth::User()->getMyConversations() == false){
				$conversations = null;
			} else {
				$allmessages = Auth::User()->getMyConversations();
				$conversationidlist = [];
				foreach($allmessages as $message){
					array_push($conversationidlist,$message->conversation_id);
				}
				$conversations = Conversation::find($conversationidlist);
			}
			
      return view('user.messages', compact('conversations'));
    }
	
		public function showReservations()
    {	
			$sales = Order::where('seller_id','=',Auth::user()->id)->get();
			$purchases = Order::where('user_id','=',Auth::user()->id)->get();
      return view('user.reservations', compact('sales','purchases'));
    }
		
		public function makePhoto(UploadedFile $file)
    {
      return Userphoto::named($file->getClientOriginalName())->move($file);
    }
	
		public function addPhoto(Request $request)
		{
      // Validate the file
      $this->validate($request, ['profilepicture' => 'mimes:jpg,jpeg,png,bmp,gif']);

      $user = Auth::User();

      $userPhoto = $this->makePhoto($request->file('profilepicture'));

      $user->addPhoto($userPhoto);
			$lastphoto = count($user->photos) -1;
      $user->update(array('image_id' => $user->photos[$lastphoto]->id));
			
			return back();
    }
		public function update(Request $request)
		{	
			
			$this->validate($request, [
				'firstname' => 'required',
				'lastname' => 'required',
			]);
			
      $user = Auth::User();
      $user->update($request->all());
			return back()->with('message', 'Je profiel is succesvol bijgewerkt!');	
    }
		public function addFBVerification(Request $request)
		{
      $user = Auth::User();
      $user->facebook_verification = $request->facebook_verification;
			$user->update();
			return '{"status":"succes"}';
    }
		public function sendEmailVerification()
		{
			$maildata = array();		
			$maildata['email'] = Auth::user()->email;
			$maildata['name'] = Auth::user()->firstname . " " . Auth::user()->lastname;
			
			Mail::queue('emails.verifyemail', ['email' => $maildata['email'], 'name' => $maildata['name']], function ($m) use ($maildata) {
      	$m->to($maildata['email'], $maildata['name'])->subject('Please verify your email on Shift');
      });
			
      return back();
    }
		public function addEmailVerification($emailkey)
		{
			$user = Auth::User();
    	if($emailkey == 'g2o0s1h5ft')
			{
				$user->email_verification = 1;
				$user->update();
			}
			return redirect('/dashboard/settings');
    }
    public function inviteFriend(Request $request)
    {
			
			$this->validate($request, [
				'email' => 'required|email|max:255|unique:users'
			]);
			
			$maildata = array();		
			$maildata['email'] = $request->email;
			$maildata['name'] = Auth::user()->firstname . " " . Auth::user()->lastname;
				
			Mail::queue('emails.invite', ['name' => $maildata['name']], function ($m) use ($maildata) {
	    	$m->to($maildata['email'], $maildata['name'])->subject("Hey, a friend of you invited you to join Shift");
	    });
			
			return back()->with("message","Uw vriend is uitgenodigd om te Shiften!");
    }
		public function readNotification(Request $request)
		{
			$notification = Notification::findOrFail($request->notification_id);
			$notification->seen = 1;
			$notification->update();
			
			$notificationsleft = Auth::user()->newNotifications()->count();
			
			return '{"status":"succes","notificationscount":' . $notificationsleft . '}';
    }
}
