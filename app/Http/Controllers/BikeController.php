<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Bike;
use App\Models\Bikephoto;
use App\Models\Bikeschedule;
use App\Models\Notification;
use App\Models\Like;
use Validator;
use Auth;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests\CreateBikeRequest;

class BikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
			$bike = new Bike; 
			
			// default values for filters
			$filterType = -1;
			$filterCity = "";
			$filtermaxprice = "";
			
			extract($_GET, EXTR_SKIP);

			if(isset($type))
			{	
				if($type != "")
				{
					$filterType = $type;
					$bike = $bike->where('type','=', $filterType);
				}
				if($city != "")
				{
					$filterCity = $city;
					$bike = $bike->where('city','LIKE', '%' . $filterCity . '%');
				}
				if($maxprice != "")
				{
					$filtermaxprice = $maxprice;
					$bike = $bike->where('price','<=', $filtermaxprice);
				}
			}
			
			$allbikes = $bike->orderBy('id','desc')->get();
			
    	return view('bikes.overview',compact('allbikes', 'filterType', 'filterCity', 'filtermaxprice'));
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
			// Init a empty bike
      $bike = new Bike;
      $bike->type = 0;
      $bike->price = "0,00";
      $bike->city = Auth::user()->city;
      $bike->street = Auth::user()->street;
      $bike->zipcode = Auth::user()->zipcode;
      $bike->streetnumber = Auth::user()->streetnumber;
			
    	return view('bikes.add',compact('bike'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBikeRequest $request)
    {
			$bike = Auth::user()->bikes()->create($request->all());
			$images = $request->bikepictures;
			if($images[0] != null) {
				$this->addPhotosFunction($images,$bike);
				$bike->update(array('image_id' => $bike->photos[0]->id));
			}
			$bike->price = str_replace(",",".",$request->price);
      $bike->update();
			$schedule = new Bikeschedule;
			$bike->schedule()->save($schedule);
      return redirect('/bikes/'. $bike->id . '/schedule/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
			$bike = Bike::where('id','=',$id)->first();
			
			if (Auth::User())
			{
				foreach($bike->comments as $comment){
					$notification = Notification::where('user_id','=',Auth::user()->id)->where('comment_id','=', $comment->id)->first();
					if (count($notification)) 
					{ 
						$notification->seen = 1;
						$notification->update();
					}
				}
				foreach($bike->likes as $like){
					$notification = Notification::where('user_id','=',Auth::user()->id)->where('like_id','=', $like->id)->first();
					if (count($notification)) 
					{ 
						$notification->seen = 1;
						$notification->update();
					}
				}
			}
			
    	return view('bikes.show',compact('bike'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $bike = Bike::findOrfail($id);
			if($bike->user_id == Auth::user()->id){
				return view('bikes.edit', compact('bike'));
			} else {
				return back();
			}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateBikeRequest $request, $id)
    {
			$bike = Bike::findOrfail($id);
    	$bike->update($request->all());
			$bike->price = str_replace(",",".",$request->price);
      $bike->update();
			return redirect('/bikes/'. $bike->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$bike = Bike::findOrfail($id);
			
			foreach ($bike->photos as $photo) {
        $photo->delete();
      }
			foreach ($bike->schedule as $schedul) {
        $schedul->delete();
      }
			foreach ($bike->orders as $order) {
				foreach ($order->notifications as $notification) {
					$notification->delete();
				}
        $order->delete();
      }
			foreach ($bike->comments as $comment) {
				foreach ($comment->notifications as $notification) {
					$notification->delete();
				}
        $comment->delete();
      }
			foreach ($bike->likes as $like) {
				foreach ($like->notifications as $notification) {
					$notification->delete();
				}
        $like->delete();
      }
			
			
			$bike->delete();
			return redirect('/bikes');
    }
		public function showaddphotos($bikeid)
		{
			$bike = Bike::findOrfail($bikeid);
			if($bike->user_id == Auth::user()->id){
				return view('bikes.addphotosview', compact('bike'));
			} else {
				return back();
			}
		}	
		
		public function addphotostobike(Request $request)
		{
			$bike = Bike::findOrfail($request->bikeid);
			$images = $request->bikepictures;
			if($images[0] != null) {
				$this->addPhotosFunction($images,$bike);
				if($bike->image()->count() == 0){
					$bike->update(array('image_id' => $bike->photos[0]->id));
				}
			}
			return redirect('/bikes/'. $bike->id);
		}
		
		public function addPhotosFunction($images,$bike)
    {
     	foreach($images as $image){
				$rules = array('file' => 'mimes:jpg,jpeg,png,bmp,gif'); 
				$validator = Validator::make(array('file'=> $image), $rules);
				if($validator->passes()){
					$bikePhoto = $this->makePhoto($image);
					$bike->addPhoto($bikePhoto);
				}
			}
    }
		
		public function makePhoto(UploadedFile $file)
    {
      return Bikephoto::named($file->getClientOriginalName())->move($file);
    }
		
		public function removePhoto(Request $request)
		{
			$bike = Bike::findOrfail($request->bikeid);
			if($bike->image->id == $request->photoid){
				Bikephoto::where('id', '=', $request->photoid)->delete();
				if($bike->photos()->count() > 0){
					$bike->update(array('image_id' => $bike->photos[0]->id));
				}
			} else {
					Bikephoto::where('id', '=', $request->photoid)->delete();
			}
			return Back();
		}
		public function showCoverphotos($bikeid)
    {
			$bike = Bike::findOrfail($bikeid);
			if($bike->user_id == Auth::User()->id) {
    		return view('bikes.coverphotos', compact('bike'));
			} else {
				return back();
			}
    }
		public function updateCoverphotos($bikeid, $photoid)
    {
			$bike = Bike::findOrfail($bikeid);
			if($bike->user_id == Auth::User()->id) {
				$bikephotoid = Bikephoto::findOrFail($photoid);

				$bike->update(array('image_id' => $bikephotoid->id));
				return redirect('/bikes/'. $bike->id);
			} else {
				return back();
			}
    }
		public function like (Request $request)
    {
      $activeuser = Auth::user();
      $bike = Bike::findOrfail($request->bikeid);
      $like = $bike->likes()->where('user_id', $activeuser->id)->first();
			$bikelikescount = $bike->likes->count();
      if ($like) {
	
				foreach ($like->notifications as $notification) {
					$notification->delete();
				}
				
        $like->delete();
				$bikelikescount = $bikelikescount -1;
				$response = '{"status":"unliked","currentlikes":"' . $bikelikescount . '"}';
      } else {
				$newlike = new Like;
				$newlike->user_id = $activeuser->id;
				$newlike->bike_id = $bike->id;
        $newlike->save();
				
				$notification = new Notification;
				$notification->user_id = $bike->user->id;
				$notification->like_id = $newlike->id;
				$notification->save();
				
				$bikelikescount = $bikelikescount +1;
				$response = '{"status":"liked","currentlikes":"' . $bikelikescount . '"}';
      }
			return $response;
    }
}
