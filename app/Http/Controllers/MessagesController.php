<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Message;
use App\Models\Conversation;
use App\Models\Notification;
use Auth;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($userid)
    {
			$user = User::findOrfail($userid);
			$activeuser = Auth::user();
			if($user->id != $activeuser->id)
			{
				$activeconversation = $activeuser->getConversationByTwoUsers($activeuser->id, $userid);
				if($activeconversation == false){
					return view('messages.create',compact('user'));
				} else {
					$conversation = Conversation::findOrfail($activeconversation);
					foreach($conversation->messages as $message)
					{
						if($message->user_id != Auth::user()->id)
						{
							$notification = Notification::where('message_id','=',$message->id)->first();
							$notification->seen = 1;
							$notification->update();
						}
					}
					return view('messages.create',compact('user','conversation'));
				}
			} else {
				return back();	
			}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			$this->validate($request, [
					'content' => 'required'
			]);
			
			$user = User::findOrfail($request->userid);
			
			if($request->conversationid) {
				$conversation = Conversation::findOrfail($request->conversationid);
			} else {
				// setup a new conversation
				$conversation = new Conversation;
				$conversation->save();
				$conversation->addParticipant($conversation->id, Auth::User()->id);
				$conversation->addParticipant($conversation->id, $user->id);
			}
			// create the new message
				$message = new Message();
				$message->conversation_id = $conversation->id;
				$message->user_id = Auth::User()->id;
				$message->content = $request->content;
				$message->save();
			
				$notification = new Notification;
				$notification->user_id = $user->id;
				$notification->message_id = $message->id;
				$notification->save();
				
				return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
