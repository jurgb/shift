<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

  protected $table = 'notifications';
  protected $fillable = ['seen'];

	 // Relationships
  public function user() {
    return $this->belongsTo('App\Models\User');
  }
	public function order() {
    return $this->belongsTo('App\Models\Order');
  }
	public function comment() {
    return $this->belongsTo('App\Models\Comment');
  }
	public function message() {
    return $this->belongsTo('App\Models\Message');
  }
	public function like() {
    return $this->belongsTo('App\Models\Like');
  }
}
