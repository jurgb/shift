<?php

namespace App\Models;

use Image;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Bikephoto extends Model
{

  protected $table = 'bikephotos';
	protected $fillable = ['path','name','thumbnail_path'];
  protected $baseDir = 'uploads/bikes/photos';

  // Relationships
  public function bike() {
    return $this->belongsTo('App\Models\Bike');
  }

  // Function
  public function delete() {

    \File::delete([
      $this->path,
      $this->thumbnail_path
    ]);

    parent::delete();
  }

  public function move(UploadedFile $file) {

    $file->move($this->baseDir, $this->name);
    Image::make($this->path)->fit(310,250)->save($this->thumbnail_path);

    return $this;

  }

  public static function named($name) {

    // Make a new instance
    $bikephoto = new static;

    return $bikephoto->saveAs($name);

  }

  protected function saveAs($name) {

    $this->name = time() . $name;
    $this->path = $this->baseDir . '/' . $this->name;
    $this->thumbnail_path = $this->baseDir . '/thumbnail/tn-' . $this->name;

    return $this;
  }

}
