<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Bike extends Model
{

  protected $table = 'bikes';

	protected $fillable = [
    'type',
		'title',
		'street',
		'streetnumber',
		'zipcode',
		'city',
		'description',
		'price',
		'pricetype',
		'image_id',
  ];


  // Relationships
  public function user() {
    return $this->belongsTo('App\Models\User');
  }
	public function photos() {
  	return $this->hasMany('App\Models\Bikephoto');
  }
	public function image() {
    return $this->belongsTo('App\Models\Bikephoto');
  }
	public function schedule() {
    return $this->hasMany('App\Models\Bikeschedule');
  }
	public function orders() {
    return $this->hasMany('App\Models\Order');
  }
	public function comments() {
    return $this->hasMany('App\Models\Comment');
  }
	public function likes() {
    return $this->hasMany('App\Models\Like');
  }
	
	// Functions
  public function addPhoto(Bikephoto $Bikephoto){
  	return $this->photos()->save($Bikephoto);
  }
}
