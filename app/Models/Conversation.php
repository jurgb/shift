<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Conversation_participants;

class Conversation extends Model
{

  protected $table = 'conversations';
  protected $fillable = [];

	 // Relationships
	public function messages() {
    return $this->hasMany('App\Models\Message');
  }
	public function participants() {
    return $this->hasMany('App\Models\Conversation_participants');
  }
	
	// Functions
	
	public function addParticipant($conversationid, $userid) {
		//add user to the conversation
		$participant = new Conversation_participants;
		$participant->conversation_id = $conversationid;
		$participant->user_id = $userid;
    $participant->save();
  }
}
