<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Conversation;
use App\Models\Notification;
use DB;
use Laravel\Cashier\Billable;


class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use Billable;
    protected $fillable = [
        'firstname',
        'lastname',
				'street',
				'streetnumber',
				'zipcode',
				'city',
				'about',
				'image_id',
				'email',
				'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
		
	
		// Relationships
		public function photos() {
      return $this->hasMany('App\Models\Userphoto');
    }
		public function image() {
      return $this->belongsTo('App\Models\Userphoto');
    }
		public function bikes() {
      return $this->hasMany('App\Models\Bike');
    }
		public function orders() {
      return $this->hasMany('App\Models\Order');
    }
		public function comments() {
      return $this->hasMany('App\Models\Comment');
    }
		public function notifications() {
      return $this->hasMany('App\Models\Notification');
    }
		public function likes() {
      return $this->hasMany('App\Models\Like');
    }
		public function messages() {
      return $this->hasMany('App\Models\Message');
    }
	
		// Functions
    public function addPhoto(Userphoto $UserPhoto){
      return $this->photos()->save($UserPhoto);
    }
	
		public function getConversationByTwoUsers($userA_id, $userB_id) {
			$results = DB::select(
            '
            SELECT cu.conversation_id
            FROM user_conversations cu
            WHERE cu.user_id=? OR cu.user_id=?
            GROUP BY cu.conversation_id
            HAVING COUNT(cu.conversation_id)=2
            '
            , [$userA_id, $userB_id]);
        if( count($results) == 1 ) 
				{
          return (int)$results[0]->conversation_id;
        } else {
					return false;	
				}
    }
		public function getMyConversations() {
			$results = DB::select(
            '
            SELECT cu.conversation_id
            FROM user_conversations cu
            WHERE cu.user_id=?
            GROUP BY cu.conversation_id
            '
            , [$this->id]);
        if( count($results) >= 1 ) 
				{
          return $results;
        } else {
					return false;	
				}
    }
		public function newNotifications() {
			
			$notifications = Notification::where('user_id','=', $this->id)->where('seen','=',0)->get();
      return $notifications;
    }
}
