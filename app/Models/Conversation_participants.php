<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation_participants extends Model
{
	public $timestamps = false;
  protected $table = 'user_conversations';
  protected $fillable = [];
	
	 // Relationships
	public function user() {
    return $this->belongsTo('App\Models\User');
  }
	public function conversation() {
    return $this->belongsTo('App\Models\Conversation');
  }
}