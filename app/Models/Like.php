<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

  protected $table = 'likes';
  protected $fillable = [];

  // Relationships
  public function user() {
    return $this->belongsTo('App\Models\User');
  }

  public function bike() {
    return $this->belongsTo('App\Models\Bike');
  }
	public function notifications() {
    return $this->hasMany('App\Models\Notification');
  }

}