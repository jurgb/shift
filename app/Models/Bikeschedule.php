<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Bikeschedule extends Model
{

  protected $table = 'bikeschedules';

	protected $fillable = [
		'monday',
		'mondaystart',
		'mondayend',
		'tuesday',
		'tuesdaystart',
		'tuesdayend',
		'wednesday',
		'wednesdaystart',
		'wednesdayend',
		'thursday',
		'thursdaystart',
		'thursday',
		'friday',
		'fridaystart',
		'fridayend',
		'saturday',
		'saturdaystart',
		'saturdayend',
		'sunday',
		'sundaystart',
		'sundayend',
  ];


  // Relationships
  public function bike() {
    return $this->belongsTo('App\Models\Bike');
  }
}
