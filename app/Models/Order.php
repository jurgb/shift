<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Order extends Model
{

  protected $table = 'orders';

	protected $fillable = [
    'quantity',
		'starttime',
		'date',
  ];


  // Relationships
  public function user() {
    return $this->belongsTo('App\Models\User');
  }
	public function bike() {
    return $this->belongsTo('App\Models\Bike');
  }
	public function notifications() {
    return $this->hasMany('App\Models\Notification');
  }
}
