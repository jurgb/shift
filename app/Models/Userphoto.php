<?php

namespace App\Models;

use Image;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Userphoto extends Model
{

  protected $table = 'userphotos';
	protected $fillable = ['path','name','thumbnail_path'];
  protected $baseDir = 'uploads/users/photos';

  // Relationships
  public function user() {
    return $this->belongsTo('App\Models\User');
  }

  // Function
  public function delete() {

    \File::delete([
      $this->path,
      $this->thumbnail_path
    ]);

    parent::delete();
  }

  public function move(UploadedFile $file) {

    $file->move($this->baseDir, $this->name);
    Image::make($this->path)->fit(310,250)->save($this->thumbnail_path);

    return $this;

  }

  public static function named($name) {

    // Make a new instance
    $userphoto = new static;

    return $userphoto->saveAs($name);

  }

  protected function saveAs($name) {

    $this->name = time() . $name;
    $this->path = $this->baseDir . '/' . $this->name;
    $this->thumbnail_path = $this->baseDir . '/thumbnail/tn-' . $this->name;

    return $this;
  }

}
