<?php

use Illuminate\Database\Seeder;

class BikeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('bikes')->insert([
      	'user_id' => '1',
				'image_id' => null,
				'type' => 'stadsfiets',
				'title' => 'Blauwe herenfiets',
				'street' => 'heidestraat',
				'streetnumber' => '88',
				'zipcode' => '2627',
				'city' => 'Schelle',
				'description' => 'Ik gebruik men fiets weinig, daarom zet ik hem te huur',
				'price' => '15,00',
				'pricetype' => null,
      ]);
			DB::table('bikes')->insert([
      	'user_id' => '2',
				'image_id' => null,
				'type' => 'mountainbike',
				'title' => 'groene herenfiets',
				'street' => 'Lange Ridderstraat',
				'streetnumber' => '44',
				'zipcode' => '2800',
				'city' => 'Mechelen',
				'description' => 'Ik gebruik men fiets weinig, daarom zet ik hem te huur',
				'price' => '15,00',
				'pricetype' => null,
      ]);
			DB::table('bikes')->insert([
      	'user_id' => '1',
				'image_id' => null,
				'type' => 'mountainbike',
				'title' => 'oranje herenfiets',
				'street' => 'Lange Ridderstraat',
				'streetnumber' => '44',
				'zipcode' => '2800',
				'city' => 'Mechelen',
				'description' => 'Ik gebruik men fiets weinig, daarom zet ik hem te huur',
				'price' => '15,00',
				'pricetype' => null,
      ]);
    }
}
