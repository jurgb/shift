<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'firstname' => 'Jurgen',
        	'lastname' => 'Barbier',
          'email' => 'jurgenbarbier@gmail.com',
          'password' => bcrypt('testing'),
					'street' => 'Heidestraat',
					'streetnumber' => '88',
					'zipcode' => '2627',
					'city' => 'Schelle',
					'about' => "I'm the lead developer of this platform, I'm not the biggest bike lover but I want to let others use my bike.",
          'admin' => '0',
        ]);
				DB::table('users')->insert([
        	'firstname' => 'Yannick',
        	'lastname' => 'Nijs',
          'email' => 'info@yannicknijs.be',
          'password' => bcrypt('testing'),
					'street' => '',
					'streetnumber' => '',
					'zipcode' => '3200',
					'city' => 'Gelrode',
					'about' => '',
          'admin' => '1',
        ]);
				DB::table('users')->insert([
        	'firstname' => 'Jens',
        	'lastname' => 'Ivens',
          'email' => 'jens.ivens@hotmail.com',
          'password' => bcrypt('testing'),
					'street' => '',
					'streetnumber' => '',
					'zipcode' => '1880',
					'city' => 'Kapelle-op-den-Bos',
					'about' => '',
          'admin' => '1',
        ]);
    }
}
