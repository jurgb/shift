<?php

use Illuminate\Database\Seeder;

class BikescheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bikeschedules')->insert([
      	'bike_id' => 1,
				'monday' => 0,
				'tuesday' => 0,
				'wednesday' => 0,
				'thursday' => 0,
				'friday' => 0,
				'saturday' => 0,
				'sunday' => 0,
      ]);
			DB::table('bikeschedules')->insert([
      	'bike_id' => 2,
				'monday' => 0,
				'tuesday' => 0,
				'wednesday' => 0,
				'thursday' => 0,
				'friday' => 0,
				'saturday' => 0,
				'sunday' => 0,
      ]);
			DB::table('bikeschedules')->insert([
      	'bike_id' => 3,
				'monday' => 0,
				'tuesday' => 0,
				'wednesday' => 0,
				'thursday' => 0,
				'friday' => 0,
				'saturday' => 0,
				'sunday' => 0,
      ]);
    }
}
