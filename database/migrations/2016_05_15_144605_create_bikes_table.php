<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes', function (Blueprint $table) {
            $table->increments('id');
					 	$table->integer('user_id')->unsigned();
					 	$table->integer('image_id')->nullable();
					 	$table->string('type')->nullable();
					 	$table->string('title')->nullable();
						$table->string('street')->nullable();
						$table->string('streetnumber')->nullable();
						$table->string('zipcode')->nullable();
						$table->string('city')->nullable();
					 	$table->longText('description')->nullable();
					 	$table->double('price', 6, 2)->nullable();
						$table->integer('pricetype')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bikes');
    }
}
