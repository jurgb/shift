<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerificationToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
        	$table->string('email_verification')->default(0);
					$table->string('facebook_verification')->default(0);
					$table->string('facebook_userid')->nullable();
					$table->string('twitter_verification')->default(0);
					$table->string('visa_verification')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
					$table->dropColumn('email_verification');
					$table->dropColumn('facebook_verification');
					$table->dropColumn('facebook_userid');
					$table->dropColumn('twitter_verification');
					$table->dropColumn('visa_verification');
        });
    }
}
