<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('street')->nullable();
            $table->string('streetnumber')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();
            $table->string('about')->nullable();
            $table->integer('image_id')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('admin')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
