<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBikescheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikeschedules', function (Blueprint $table) {
          $table->increments('id');
					$table->integer('bike_id')->unsigned();
					$table->string('monday')->default(0);
					$table->string('tuesday')->default(0);
					$table->string('wednesday')->default(0);
					$table->string('thursday')->default(0);
					$table->string('friday')->default(0);
					$table->string('saturday')->default(0);
					$table->string('sunday')->default(0);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bikeschedules');
    }
}
