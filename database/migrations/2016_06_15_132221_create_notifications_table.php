<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
						$table->integer('user_id')->nullable();
						$table->integer('order_id')->nullable();
						$table->integer('comment_id')->nullable();
						$table->integer('message_id')->nullable();
						$table->integer('like_id')->nullable();
						$table->integer('friendrequest_id')->nullable();
            $table->string('seen')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
