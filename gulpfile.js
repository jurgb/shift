var elixir = require('laravel-elixir');
var gulp = require("gulp");
var cleanCSS = require('gulp-clean-css');
var imagemin = require('gulp-imagemin');
var jsmin = require('gulp-jsmin');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');

elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Gulp tasks
 |--------------------------------------------------------------------------
*/

gulp.task('minify-css', function() {
    return gulp.src('public/css/app.css')
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(gulp.dest('public/css/'));
});
 
gulp.task('imagemin', () =>
	gulp.src('public/images/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('public/images'))
);

gulp.task('scripts', function() {
  return gulp.src('public/js/custom/*.js')
    .pipe(concat('app.js'))
    .pipe(gulp.dest('public/js/'));
});

gulp.task('jsmin', function () {
	gulp.src('public/js/app.js')
		.pipe(jsmin())
		.pipe(gulp.dest('public/js'));
});

gulp.task('minify-appjs', function() {
  runSequence('scripts', 'jsmin');
});


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss').task('minify-css').task('imagemin').task('minify-appjs');
});