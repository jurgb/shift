$(document).ready(function(){
	$('.btn_like_bike').click(function(e){
		var buttonClicked = $(this);
		
    var response = $.ajax({
			url: buttonClicked.attr('href'),
			data: {'_token':$('meta[name="_token"]').attr('content')},
      type: "post",	
			dataType: "json",
      success: function(data){
				if (data.status == "liked"){
					buttonClicked.html(data.currentlikes + " <i class='fa fa-heart'></i>");
				} else {
					buttonClicked.html(data.currentlikes + " <i class='fa fa-heart-o'></i>");
				}
      },
			error: function(errres){
				alert("Er ging its mis, herlaad de pagina en probeer opnieuw! Onze excuses voor dit ongemak.");
			}
    });
		e.preventDefault();
  }); 
});