var element =  document.getElementById('imgInp');
if (typeof(element) != 'undefined' && element != null)
{
	window.onload = function(){
	//Check File API support
		if(window.File && window.FileList && window.FileReader)
		{
			var filesInput = document.getElementById("imgInp");
			filesInput.addEventListener("change", function(event){
			var files = event.target.files; //FileList object
			var output = document.getElementById("image_previewer");
			output.innerHTML ="<h2 class='formheader'>Hier is een overzicht van alle foto's die je gaat toegevoegen</h2>";
			for(var i = 0; i< files.length; i++)
			{
				var file = files[i];
				//Only pics
				if(!file.type.match('image'))
				continue;
				var picReader = new FileReader();
				picReader.addEventListener("load",function(event){
					var picFile = event.target;
					var div = document.createElement("div");
					div.style.backgroundImage = "url(" + picFile.result + ")";
					div.style.backgroundSize = "cover";
					div.style.backgroundPosition = "center center";
					div.style.display = 'inline-block';
					div.style.margin = '20px';
					div.style.width = '200px';
					div.style.height = '200px';
					output.insertBefore(div,null);
				});
				//Read the image
				picReader.readAsDataURL(file);
			}
			});
		}
		else
		{
		console.log("Your browser does not support File API");
		}
	}
}